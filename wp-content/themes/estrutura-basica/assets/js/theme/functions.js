/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

window.onload = async () => {
	//$(".historia .video").hide();
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			setTimeout(() => {
				Swal.fire({
					icon: 'success',
					title: 'Sucesso!',
					text: $('.wpcf7-response-output').html(),
				})
			}, 100);
		})

		form.addEventListener('wpcf7mailfailed', () => {
			setTimeout(() => {
				Swal.fire({
					icon: 'error',
					title: 'Ocorreu um erro!',
					text: $('.wpcf7-response-output').html(),
				})
			}, 100);

		})
	}

	var width = $(window).width()
    if (width >= 1024) {
        var menu = document.getElementById("menu-bars");
		menu.addEventListener("mouseenter", function() {
			$(this).find(".hover").css('opacity', '1');
		});
		menu.addEventListener("mouseleave", function() {
			$(this).find(".hover").css('opacity', '0');
		});
    }
	var width = $(window).width()
    if (width >= 1024) {
        var menu = document.getElementById("close");
		menu.addEventListener("mouseenter", function() {
			$(this).find(".hover").css('opacity', '1');
		});
		menu.addEventListener("mouseleave", function() {
			$(this).find(".hover").css('opacity', '0');
		});
    }

	window.addEventListener('click', function(e){
        if (!document.getElementById('menu-occ').contains(e.target) && !document.getElementById('menu-bars1').contains(e.target) && !document.getElementById('menu-bars2').contains(e.target)){
            if ($('.menu-occ.active').length > 0) {
				$(".conditionalOpen, .conditionalClose").toggleClass("conditionalOpen conditionalClose")
                setTimeout(function(){
					$('.menu-occ').removeClass('active');
            		$("#menu-bars").removeClass('active');
				}, 1000);
            }
        }
        if (document.getElementById('overlay').contains(e.target) ) {
            if ($('.menu-occ.active').length > 0) {
				$(".conditionalOpen, .conditionalClose").toggleClass("conditionalOpen conditionalClose")
                setTimeout(function(){
					$('.menu-occ').removeClass('active');
            		$("#menu-bars").removeClass('active');
				}, 1000);
            }
        }
		if (document.getElementById('close').contains(e.target) ) {
            if ($('.menu-occ.active').length > 0) {
				$(".conditionalOpen, .conditionalClose").toggleClass("conditionalOpen conditionalClose")
				setTimeout(function(){
					$('.menu-occ').removeClass('active');
            		$("#menu-bars").removeClass('active');
				}, 1000);
            }
        }
    });

	$("#menu-bars").on( "click", function() {
        if ($('.menu-occ.active').length > 0) {
			$(".conditionalOpen, .conditionalClose").toggleClass("conditionalOpen conditionalClose");
            $('.menu-occ').removeClass('active');
            $("#menu-bars").removeClass('active');
        } else {
			$(".conditionalOpen, .conditionalClose").toggleClass("conditionalOpen conditionalClose");
            $('.menu-occ').addClass('active');
            $("#menu-bars").addClass('active');
        }
    });

	if (width < 769) {
		$(".menu-item-has-children").on( "click", function() {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active');
				$(this).find('.sub-menu').slideToggle();
			} else {
				$(this).addClass('active');
				$(this).find('.sub-menu').slideToggle();
			}
		});
	}

	//slider single produtos
	var slider_single = $('.light-galeria').lightSlider({
		item: 1,
		speed: 1000,
		auto: true,
		loop: true,
		pause: 4000,
		keyPress: true,
		controls: true,
		slideMargin: 0,
		onSliderLoad: function (el) {
			$('.light-galeria').removeClass('cS-hidden');
		},
	})
	//slider home
	var rev_home = $('#rev-home .slider-container').lightSlider({
		item: 1,
		speed: 1000,
		pause: 10000,
		auto: true,
		loop: true,
		vertical: true,
		keyPress: true,
		controls: true,
		slideMargin: 0,
	})

	var slidesporvez1 = 6;
	var slidesporvez2 = 5;
	if (width < 1280) {
		slidesporvez1 = 5;
	}
	if (width < 1024) {
		slidesporvez1 = 4;
		slidesporvez2 = 4;
	}
	if (width < 481) {
		slidesporvez1 = 2;
		slidesporvez2 = 2;
	}
	var swiper = new Swiper('.mentores .swiper-container', {
		slidesPerView: slidesporvez1,
		slidesPerColumn: 3,
		spaceBetween: 15,
		slidesPerColumnFill: 'row',
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
	swiper.on('slideChange', function () {
		if ($('.mentores .swiper-container .swiper-button-prev').hasClass('swiper-button-disabled')) {
			$(".mentores .prev").addClass('disabled');
		}else{
			$(".mentores .prev").removeClass('disabled');
		}
		if ($('.mentores .swiper-container .swiper-button-next').hasClass('swiper-button-disabled')) {
			$(".mentores .next").addClass('disabled');
		}else{
			$(".mentores .next").removeClass('disabled');
		}
	});
	if ($('.mentores .swiper-container .swiper-button-prev').hasClass('swiper-button-disabled')) {
		$(".mentores .prev").addClass('disabled');
	}else{
		$(".mentores .prev").removeClass('disabled');
	}
	if ($('.mentores .swiper-container .swiper-button-next').hasClass('swiper-button-disabled')) {
		$(".mentores .next").addClass('disabled');
	}else{
		$(".mentores .next").removeClass('disabled');
	}
	$(".mentores .next").on( "click", function() {
        $('.mentores .swiper-container .swiper-button-next').click();
    });
	$(".mentores .prev").on( "click", function() {
        $('.mentores .swiper-container .swiper-button-prev').click();
    });

	var swiper2 = new Swiper('.timeline .swiper-container', {
		slidesPerView: slidesporvez2,
		hashNavigation: {
			watchState: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
	swiper2.on('slideChange', function () {
		if ($('.timeline .swiper-container .swiper-button-prev').hasClass('swiper-button-disabled')) {
			$(".timeline .prev").addClass('disabled');
		}else{
			$(".timeline .prev").removeClass('disabled');
		}
		if ($('.timeline .swiper-container .swiper-button-next').hasClass('swiper-button-disabled')) {
			$(".timeline .next").addClass('disabled');
		}else{
			$(".timeline .next").removeClass('disabled');
		}
	});
	if ($('.timeline .swiper-container .swiper-button-prev').hasClass('swiper-button-disabled')) {
		$(".timeline .prev").addClass('disabled');
	}else{
		$(".timeline .prev").removeClass('disabled');
	}
	if ($('.timeline .swiper-container .swiper-button-next').hasClass('swiper-button-disabled')) {
		$(".timeline .next").addClass('disabled');
	}else{
		$(".timeline .next").removeClass('disabled');
	}
	$(".timeline .next").on( "click", function() {
        $('.timeline .swiper-container .swiper-button-next').click();
    });
	$(".timeline .prev").on( "click", function() {
        $('.timeline .swiper-container .swiper-button-prev').click();
    });

	if (width < 481) {
		//slider socio mobile
		var slider_single = $('.socios-container').lightSlider({
			item: 1,
			speed: 2000,
			auto: true,
			loop: true,
			pause: 4000,
			keyPress: true,
			controls: true,
			slideMargin: 0,
			onSliderLoad: function (el) {
				$('.socios-container').removeClass('cS-hidden');
			},
		})
	}

	$('.portfolio .right #filtro').on("change", function(){
		if ($(this).val() == 'todos') {
			$('#portfolio .case').removeClass('portOpen');
			$('#portfolio .case').addClass('portClose');
			$('#portfolio .case').css('display', 'none');
			setTimeout(function(){
				$('#portfolio .case').removeClass('portClose');
				$('#portfolio .case').addClass('portOpen');
				$('#portfolio .case').css('display', 'flex');
			}, 100);
		} else {
			$('#portfolio .case').removeClass('portOpen');
			$('#portfolio .case').addClass('portClose');
			$('#portfolio .case').css('display', 'none');
			var $this = $(this);
			setTimeout(function(){
				$('.'+$this.val()).removeClass('portClose');
				$('.'+$this.val()).addClass('portOpen');
				$('.'+$this.val()).css('display', 'flex');
			}, 100);
		}
	});

	//categorias
	// $('.blog.listagem #filtro').on("change", function(){
	// 	$('.blog.listagem #buscaheader .button').click();
	// });
	// possivel filtragem em tempo real
	// $('.blog.listagem .right #filtro').on("change", function(){
	// 	if ($(this).val() == 'todos') {
	// 		$('.noticias .artigo').removeClass('portClose');
	// 		$('.noticias .artigo').addClass('portOpen');
	// 		$('.noticias .artigo').css('display', 'inline-block');
	// 	} else {
	// 		$('.noticias .artigo').removeClass('portOpen');
	// 		$('.noticias .artigo').addClass('portClose');
	// 		$('.noticias .artigo').css('display', 'none');
	// 		$('.'+$(this).val()).removeClass('portClose');
	// 		$('.'+$(this).val()).addClass('portOpen');
	// 		$('.'+$(this).val()).css('display', 'inline-block');
	// 	}
	// });

	// $('.left.home .centro >a').on( "click", function() {
	// 	$(this).parent().parent().parent().addClass('clickesq');
	// 	$(this).parent().parent().css({"position": "absolute", "left": "50%", "bottom": "50px"});
    // });

	// $('.right.home .centro >a').on( "click", function() {
	// 	$(this).parent().parent().parent().addClass('clickdir');
	// 	$(this).parent().parent().css({"position": "absolute", "left": "50%", "bottom": "50px"});
    // });

	$(".right.home .left-right-content >div").on( "click", function() {
		// $(this).addClass('clickdir');
		// $(this).find('a.curriculo').css({"position": "absolute", "left": "50%", "bottom": "50px"});
		location.href = $(this).find('a.curriculo').attr('href');
	}).children(".linguagem").click(function(e) {
		e.stopPropagation();
	});

	$(".left.home .left-right-content >div").on( "click", function() {
		// $(this).addClass('clickesq');
		// $(this).find('a.curriculo').css({"position": "absolute", "left": "50%", "bottom": "50px"});
		location.href = $(this).find('a.curriculo').attr('href');
	}).children(".social").click(function(e) {
		e.stopPropagation();
	});

	$('.a2a_button_facebook').html('<i class="fab fa-facebook-f"></i>');
    $('.a2a_button_twitter').html('<i class="fab fa-twitter"></i>');
    $('.a2a_button_linkedin').html('<i class="fab fa-linkedin-in"></i>');
    $('.addtoany_share').html('<i class="fas fa-paperclip"></i>');
	$('.share.hidden').removeClass('hidden');

	//smooth scrolling
	$(document).ready(function(){
		"use strict"; // Start of use strict
		$('a').bind('click', function(event) {
			if (this.pathname == window.location.pathname &&
				this.protocol == window.location.protocol &&
				this.host == window.location.host) {
				if (this.hash !== "") {
					event.preventDefault();
					var hash = this.hash;
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 800, function(){
						window.location.hash = hash;
					});
				}
			}
		});
	});

	if($(".floating .sociais").length > 0){
		var destino = $(".floating .sociais").attr('href');
		var url = window.location.href;
		var motivo = url.split("/")[url.split("/").length-2];
		$(".floating .sociais").attr('href', destino+'?motivo='+motivo);
	}

	if($("section.al-container.single.case .button").length > 0){
		var destino = $("section.al-container.single.case .button").attr('href');
		var url = window.location.href;
		var startup = url.split("/")[url.split("/").length-2];
		$("section.al-container.single.case .button").attr('href', destino+'?startup='+startup);
	}

	//Mascaras
	$('input[name="telefone"]').mask('(00) 00000-0000');
	$('input[name="cnpj"]').mask('00.000.000/0000-00');
	$('input[name="cpf"]').mask('000.000.000-00');
	//$('input[name="email"]').mask('A', {
	//	translation: {
	//		"A": { pattern: /[\w@\-.+]/, recursive: true }
	//	}
	//});

	$(".estrangeiro input").on( "click", function() {
		$(".cpf").parent().fadeToggle().css("display","inline-block");
		$(".pais").parent().fadeToggle().css("display","inline-block");
		$(".passaporte").parent().fadeToggle().css("display","inline-block");
	});

	$('.trp-language-wrap a img').each(function(){
		let $class = '';
		let $this = $(this);
		if ($(this).parent().hasClass('trp-ls-disabled-language')) {
			$class = 'active';
		}
		$('.linguagem').append('<a class="'+$(this).attr('alt').split('_')[1]+' '+$class+'">'+$(this).attr('alt').split('_')[1]+'</a>')
		$('.linguagem .'+$(this).attr('alt').split('_')[1]).on( "click", function() {
			$('.linguagem a').removeClass('active');
			$(this).addClass('active');
			$this.click();
		});
		$('.linguagem').css('visibility', 'visible');
	});
	var newsletter_form = $('.newsletter-occ');
	$(".newsletter .news").on( "click", function() {
		Swal.fire({
			title: '<h1 class="title alt">Assine nossa Newsletter</h1>',
			html:'<div id="swal-newsletter"></div>',
			width: '90%',
			showCloseButton: true,
			showCancelButton: false,
			showConfirmButton: false,
			allowOutsideClick: false
		})
		$('.swal2-content #swal2-content #swal-newsletter').append(newsletter_form);
	});
	$('.valores input').each(function(){
		var oldId =$(this).attr("id");
		var id = $(this).closest('.wpcf7').attr('id');
		$(this).attr("id", oldId+"-"+id)
	});
	$('.valores label').each(function(){
		var oldId =$(this).attr("for");
		var id = $(this).closest('.wpcf7').attr('id');
		$(this).attr("for", oldId+"-"+id)
	});
	$('.wpcf7-hidden').each(function(){
		var id = $(this).closest('.wpcf7').attr('id');
		$(this).attr("id", "hidden-"+id)
	});
	$('.valores input').each(function(){
		var valores = $(this);
		var id = $(this).closest('.wpcf7').attr('id');
		$(this).on( "click", function() {
			$('#'+id+' label .left').removeClass('active');
			$(this).parent().parent().find('label .left').addClass('active');
			$('.wpcf7-hidden#hidden-'+id).each(function(){
				$(this).val($(valores).val());
			});
		});
	});
	$('.call-to-form').each(function(){
		$(this).on("click", function(){
			$(".topo .button-form-lp").removeClass("active");
			$('.wpcf7').removeClass("active");
			$('.button-form-lp[id*="'+$(this).attr("id")+'"]').addClass("active");
			$('.wpcf7[id*="'+$(this).attr("id")+'"]').addClass("active");
			document.querySelector('.al-container.lp.formulario').scrollIntoView({ behavior: "smooth" });
		});
	});
	$(".topo .button-form-lp").each(function(){
		$(this).on("click", function(){
			$(".topo .button-form-lp").removeClass("active");
			$('.wpcf7').removeClass("active");
			$(this).addClass("active");
			$('.wpcf7[id*="'+$(this).attr("id")+'"]').addClass("active");
		});
	});
	$(".topo .button-form-lp:first-of-type").each(function(){
		$(this).addClass("active");
		$('.wpcf7[id*="'+$(this).attr("id")+'"]').addClass("active");
	});
	$(".historia .video").hide("slow");
	$("#close-video").hide("slow");
	$(".historia .right").on("click", function(){
		$(".historia .video").show("slow");
		$(".mejs-overlay-button").click();
		$("#close-video").show("slow");
	});
	$("#close-video").on("click", function(){
		$(".historia .video").hide("slow");
		$("#close-video").hide("slow");
	});
	if($(".page-template-template-home-new")){
		$(".page-template-template-home-new .main-header").removeClass("exeption");
		$(".page-template-template-home-new #Menu_1_ path").css("fill", "#FFF");
		$(".page-template-template-home-new .logo-holder img").attr("src", "/wp-content/themes/estrutura-basica/assets/images/logo-3.png");
	}
	document.addEventListener('scroll', function() {
		if(window.scrollY >= 100) {
			$(".page-template-template-home-new .main-header").addClass("exeption");
			$(".page-template-template-home-new #Menu_1_ path").css("fill", "#ff6b19");
			$(".page-template-template-home-new .logo-holder img").attr("src", "/wp-content/themes/estrutura-basica/assets/images/logo.png");
			$("body.page-template-template-home-new .main-header .linguagem").css("text-shadow", "none");
		}else{
			$(".page-template-template-home-new .main-header").removeClass("exeption");
			$(".page-template-template-home-new #Menu_1_ path").css("fill", "#FFF");
			$(".page-template-template-home-new .logo-holder img").attr("src", "/wp-content/themes/estrutura-basica/assets/images/logo-3.png");
			$("body.page-template-template-home-new .main-header .linguagem").css("text-shadow", "0px 0px 6px black");
		}
	}, true);
	$(".depoimento").on("click", function(){
		Swal.fire({
			imageUrl: $(this).find(".img-container img").attr("src"),
			imageHeight: 300,
			imageAlt: 'A tall image',
			html: $(this).find(".content").html(),
			title: $(this).find(".left h3").text(),
		})
	});
}
