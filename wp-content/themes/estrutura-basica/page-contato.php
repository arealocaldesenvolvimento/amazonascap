<?php get_header();?>
<div class="back1">
    <section class="al-container contato" role="main">
        <?php while (have_posts()): the_post() ?>
            <article id="id-<?php the_ID() ?>" <?php post_class() ?>>
                <header class="entry-title">
                    <h1 class="title"><?php the_title() ?></h1>
                </header>
                <div class="form">
                    <?= do_shortcode('[contact-form-7 id="367" title="Contato"]'); ?>
                </div>
            </article>
        <?php endwhile ?>
    </section>
</div>
<?php if ($_GET['motivo'] != null): ?>
<script>
    jQuery("span.wpcf7-form-control-wrap.motivo >select").prepend('<option value="<?= $_GET['motivo'] ?>"><?= $_GET['motivo'] ?></option>');
    jQuery('select[name="motivo"]').val("<?= $_GET['motivo'] ?>");
</script>
<?php endif; ?>

<?php get_footer(); ?>
