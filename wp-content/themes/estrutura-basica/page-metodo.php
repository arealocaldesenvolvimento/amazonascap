<?php get_header() ?>
<div class="back1">
    <section class="al-container metodo universitaria" id="universitaria">
        <div class="left">
            <h1 class="title">Nosso<br>método</h1>
            <h1 class="title alt">Aceleração<br>universitária</h1>
            <p><?= get_field('primeiro_paragrafo') ?></p>
            <p><?= get_field('segundo_paragrafo') ?></p>
            <a class="button" href="<?= get_site_url() ?>/contato">Quer acelerar seus alunos?</a>
        </div>
        <div class="right">
            <img src="<?= get_field('imagem')['url'] ?>" alt="<?= get_field('imagem')['title'] ?>">
        </div>
    </section>
</div>
<div class="back2">
    <section class="al-container metodo early" id="early-stage"> 
        <h1 class="title">aceleração<br>early stage</h1>
        <?php if( have_rows('early_stage') ): ?>
        <ul>
            <?php while( have_rows('early_stage') ): the_row(); 
            $image = get_sub_field('icone');
            ?>
            <li>
                <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <p><?= get_field('early_stage_texto') ?></p>
        <div class="acelerar">
            <a href="<?= get_site_url() ?>/contato" class="button">Quer acelerar sua startup?</a>
        </div>
    </section>
</div>
<div class="back3">
    <section class="al-container metodo aberta" id="aberta">
        <h1 class="title alt">inovação<br>aberta</h1>
        <?php if( have_rows('inovacao_aberta') ): ?>
        <ul>
            <?php while( have_rows('inovacao_aberta') ): the_row(); 
            $imagem = get_sub_field('icone');
            ?>
            <li>
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                <b><?php the_sub_field('titulo'); ?></b>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <div class="left">
            <p><?= get_field('inovacao_aberta_texto') ?></p>
        </div>
        <div class="right">
            <a href="<?= get_site_url() ?>/contato" class="button">Quer acelerar sua empresa?</a>
        </div>
    </section>
</div>
<div class="back2">
    <section class="al-container metodo early" id="secao4"> 
        <h1 class="title">aceleração<br>early stage</h1>
        <?php if( have_rows('secao_4') ): ?>
        <ul>
            <?php while( have_rows('secao_4') ): the_row(); 
            $imagem = get_sub_field('icone');
            ?>
            <li>
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                <b><?php the_sub_field('titulo'); ?></b>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <p><?= get_field('secao_4_texto') ?></p>
        <div class="acelerar">
            <a href="<?= get_site_url() ?>/contato" class="button">Quer acelerar sua startup?</a>
        </div>
    </section>
</div>
<?php get_footer() ?>