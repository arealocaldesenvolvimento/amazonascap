<?php get_header() ?>
    <section class="al-container portfolio" id="startups">
        <div class="left">
            <h1 class="title"><?= !empty(get_the_title(1263)) ? get_the_title(1263) : get_the_title(539) ?></h1>
       </div>
        <div class="right">
            <select name="filtro" id="filtro">
                <option value="" disabled="" selected="">FILTRAR POR</option>
                <option value="todos">Todos</option>
                <?php $terms = get_terms([
                    'taxonomy' => 'categoria',
                    'hide_empty' => true,
                    'orderby' => 'title',
                    'order' => 'ASC',
                ]);
                foreach ($terms as $key => $categoria): ?>
                    <option value="<?= $categoria->slug ?>"><?= $categoria->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php
        $startups = new WP_Query(array(
            'post_type' => 'startups',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        )); ?>
        <?php if (!empty(apply_filters('the_content', get_post_field('post_content', 1263)))) {
            $content = apply_filters('the_content', get_post_field('post_content', 1263));
        }else{
            $content = apply_filters('the_content', get_post_field('post_content', 539));
        }
        echo $content; ?>
        <div id="portfolio">
            <?php if ($startups->have_posts()): ?>
                <?php while($startups->have_posts()) : $startups->the_post(); ?>
                    <div class="case portOpen <?= get_the_terms(get_the_ID(), 'categoria')[0]->slug ?>">
                        <a href="<?= get_the_permalink() ?>">
                            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="logo">
                        </a>
                    </div>
                <?php endwhile; 
            endif; ?>
        </div>
    </section>
<?php get_footer() ?>
