<?php get_header() ?>
<div class="back1">
    <section class="al-container contato" role="main">
        <?php while (have_posts()): the_post() ?>
            <article id="id-<?php the_ID() ?>" <?php post_class() ?>>
                <header class="entry-title">
                    <h1 class="title"><?php the_title() ?></h1>
                </header>
                <?php the_content(); ?>
                <div class="form">
                    <?= do_shortcode('[contact-form-7 id="368" title="Contato do investidor"]'); ?>    
                </div>
            </article>
        <?php endwhile ?>
    </section>
</div>
<?php if ($_GET['startup'] != null): ?>
<script>
    jQuery('input#startup').val("<?= $_GET['startup'] ?>");
</script>
<?php endif; ?>
<?php get_footer() ?>