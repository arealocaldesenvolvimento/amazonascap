<?php get_header(); ?>
<div class="table">
<div class="left home">
  <div class="left-right-content">
    <div class="left">
      <div class="social">
        <a class="sociais" target="blank" href="https://twitter.com/amazonascap">
        <i class="fab fa-twitter"></i></a>
        <a class="sociais" target="blank" href="https://www.linkedin.com/company/amazonascap-aceleradora">
        <i class="fab fa-linkedin-in"></i></a>
        <a class="sociais" target="blank" href="https://www.instagram.com/amazonascap/">
        <i class="fab fa-instagram"></i></a>
        <a class="sociais" target="blank" href="https://www.youtube.com/channel/UCdAvBpHbAVYEBo7a1kyOV-g/videos">
        <i class="fab fa-youtube"></i></a>
      </div>
      <div class="container">
        <h1>Quem Somos</h1>
        <div class="centro"><a href="<?= get_site_url() ?>/quem-somos"  class="curriculo verde"><i class="fas fa-plus"></i></a></div>
      </div>
    </div>
    <div class="right">
      <div class="container">
        <h1>Nossos Projetos</h1>
        <div class="centro"><a href="<?= get_site_url() ?>/nossos-projetos" class="curriculo laranja"><i class="fas fa-plus"></i></a></div>
      </div>
    </div>
  </div>
</div>

<div class="conteudo home">
  <div class="logo-holder">
    <img src="<?= get_image_url('logo-branca.png') ?>" alt="AmazonasCap">
  </div>
  <div id="rev-home">
    <div class="container2">
    <div class="slider-container">
      <?php $blog = new WP_Query(array(
          'post_type' => 'post',
          'posts_per_page' => 3,
          'orderby' => 'date',
          'meta_query' => array(
            array(
                'key'   => 'destaque',
                'value' => '1',
            )
            ),
          'order' => 'desc'
      ));
      $index = 0;
      while($blog->have_posts()) : $blog->the_post(); 
      $index++; 
      $class = '';
      if ($index % 3 == 0) {
        $class = 'third';
      }elseif ($index % 2 == 0) {
        $class = 'even';
      }else {
        $class = 'odd';
      }?>
      <div class="article-container">
          <article class="clearfix <?= $class ?>" id="id-<?php the_ID(); ?>">
              <a href="<?= get_the_permalink() ?>" title="<?=  get_the_title() ?>">
                  <span class="titulo"><?=  get_the_title() ?></span>
                  <!-- <img src="<?=  get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?=  get_the_title() ?>"/> -->
              </a>
              <header>
          </article>
      </div>
      <?php endwhile; 
      wp_reset_postdata();?>
    </div>
    </div>
  </div>
</div>

<div class="right home">
  <div class="left-right-content">
    <div class="left">
    <div class="linguagem"></div>
      <div class="container">
        <h1>Nossos Métodos</h1>
        <div class="centro"><a href="<?= get_site_url() ?>/nossos-metodos" class="curriculo laranja"><i class="fas fa-plus"></i></a></div>
      </div>
    </div>
    <div class="right">
      <div class="social">
        <a class="sociais" href="<?= get_site_url() ?>/contato">
        <i class="fas fa-comments"></i>
        </a>
      </div>
      <div class="container">
        <h1>Nosso Portfólio</h1>
        <div class="centro"><a href="<?= get_site_url() ?>/startups" class="curriculo verde"><i class="fas fa-plus"></i></a></div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
