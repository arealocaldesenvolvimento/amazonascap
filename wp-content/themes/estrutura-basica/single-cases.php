<?php get_header() ?>
<div class="back1">
    <section class="al-container single case">
        <h1 class="title">Nossos portfólio</h1>
        <div class="left">
            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="logo">
            <?php the_content(); ?>
            <a href="<?= get_site_url() ?>/contato-do-investidor" class="button">requisição de plano de negócio</a>
        </div>
        <div class="right">
            <iframe src="<?='https://www.youtube.com/embed/'.get_field('video')?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>
</div>
<?php get_footer() ?>
