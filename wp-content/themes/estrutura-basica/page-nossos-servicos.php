<?php get_header();?>
<div class="back2">
    <section class="al-container metodo early" id="early-stage">
		<h1 class="title">Nossos<br>serviços</h1>
		<h1 class="title alt"><?= get_field('secao_2_titulo') ?></h1>
        <?php if( have_rows('early_stage') ): ?>
        <ul>
            <?php while( have_rows('early_stage') ): the_row();
            $image = get_sub_field('icone');
            ?>
            <li>
                <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <p><?= get_field('early_stage_texto') ?></p>
        <div class="acelerar">
            <a href="https://amazonascap.com.br/aceleracao-de-startups/" class="button"><?= get_field('secao_2_botao') ?></a>
        </div>
    </section>
</div>
<div class="back1">
    <section class="al-container metodo universitaria" id="universitaria">
        <div class="left">
            <h1 class="title alt"><?= get_field('secao_1_titulo') ?></h1>
            <p><?= get_field('primeiro_paragrafo') ?></p>
            <p><?= get_field('segundo_paragrafo') ?></p>
        </div>
        <div class="right">
            <img src="<?= get_field('imagem')['url'] ?>" alt="<?= get_field('imagem')['title'] ?>">
        </div>
        <div class="acelerar center">
            <a class="button" href="https://amazonascap.com.br/aceleracao-de-projetos-universitarios/"><?= get_field('secao_1_botao') ?></a>
        </div>
    </section>
</div>
<div class="back2">
    <section class="al-container metodo aberta" id="aberta">
        <h1 class="title alt"><?= get_field('secao_3_titulo') ?></h1>
        <?php if( have_rows('inovacao_aberta') ): ?>
        <ul>
            <?php while( have_rows('inovacao_aberta') ): the_row();
            $imagem = get_sub_field('icone');
            ?>
            <li>
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                <b><?php the_sub_field('titulo'); ?></b>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <div class="all">
            <p><?= get_field('inovacao_aberta_texto') ?></p>
        </div>
        <div class="acelerar center">
            <a href="https://amazonascap.com.br/programas-de-inovacao-aberta" class="button"><?= get_field('secao_3_botao') ?></a>
        </div>
    </section>
</div>
<div class="back1">
    <section class="al-container metodo early" id="secao4">
        <h1 class="title"><?= get_field('secao_4_titulo') ?></h1>
        <?php if( have_rows('secao_4') ): ?>
        <ul>
            <?php while( have_rows('secao_4') ): the_row();
            $imagem = get_sub_field('icone');
            ?>
            <li>
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                <b><?php the_sub_field('titulo'); ?></b>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <p><?= get_field('secao_4_texto') ?></p>
        <div class="acelerar">
            <a href="https://amazonascap.com.br/aceleracao-internacionalizacao" class="button"><?= get_field('secao_4_botao') ?></a>
        </div>
    </section>
</div>
<div class="back2">
    <section class="al-container metodo investimento" id="secao5">
        <h1 class="title"><?= get_field('secao_5_titulo') ?></h1>
        <?php if( have_rows('secao_5') ): ?>
        <ul>
            <?php while( have_rows('secao_5') ): the_row();
            $imagem = get_sub_field('icone');
            ?>
            <li>
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                <b><?php the_sub_field('titulo'); ?></b>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <p><?= get_field('secao_5_texto') ?></p>
        <div class="acelerar center">
            <a href="https://amazonascap.com.br/invista-em-startups-de-grande-potencial" class="button"><?= get_field('secao_5_botao') ?></a>
        </div>
    </section>
</div>
<div class="back1">
    <section class="al-container metodo edtech" id="edtechs">
        <h1 class="title"><?= get_field('secao_6_titulo') ?></h1>
		<h2 class="subtitle">Produtos, serviços e consultoria especializada em Educação.</h2>
        <?php if( have_rows('secao_6') ): ?>
        <ul>
            <?php while( have_rows('secao_6') ): the_row();
            $imagem = get_sub_field('icone');
            ?>
            <li>
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                <b><?php the_sub_field('titulo'); ?></b>
                <p><?php the_sub_field('texto'); ?></p>
            </li>
            <?php endwhile; ?>
        </ul>
        <?php endif; ?>
        <p><?= get_field('secao_6_texto') ?></p>
        <div class="acelerar center">
            <a href="https://amazonascap.com.br/contato/" class="button"><?= get_field('secao_6_botao') ?></a>
        </div>
    </section>
</div>
<?php get_footer() ?>
