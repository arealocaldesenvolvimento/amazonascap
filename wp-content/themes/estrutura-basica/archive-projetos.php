<?php get_header() ?>
<div class="back1">
    <section class="al-container projetos similar" id="nossos-projetos">
        <div class="left">
            <h1 class="title">Nossos projetos</h1>
            <?php $content = apply_filters('the_content', get_post_field('post_content', 357));
            echo $content; ?>
        </div>
        <div class="right">
            <div class="button-container">
                <a class="button" href="<?= get_site_url() ?>/contato">Precisa de um projeto similar?</a>
            </div>
        </div>
    </section>
    <?php
    $projetos = new WP_Query(array(
        'post_type' => 'projetos',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC'
    )); ?>
    <div id="portfolio">
    <?php if ($projetos->have_posts()): 
        $index = 0;?>
        <?php while($projetos->have_posts()) : $projetos->the_post(); 
        if ($index % 2 == 0): ?>
            <div class="background par">
                <section class="al-container projetos projeto">
                    <div class="ancor" id="id-<?= get_the_ID()?>"></div>
                    <div class="left">
                        <div class="img-container">
                            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="logo">
                        </div>
                        <h3><?= get_the_title() ?></h3>
                        <p><?php the_content(); ?></p>
                    </div>
                    <div class="right">
                    <?php $images = get_field('galeria');
                    if( $images ): ?>
                        <div class="light-galeria">
                            <?php foreach( $images as $image ): ?>
                                <div class="galeria-container">
                                    <div class="galeria-container2">
                                        <img src="<?= esc_url($image['url']) ?>" alt="<?= esc_attr($image['alt'])?>">
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div> 
                    <?php endif; ?>
                    </div>
                </section>
            </div>
        <?php else: ?>
            <div class="background impar">
                <section class="al-container projetos projeto">
                    <div class="ancor" id="id-<?= get_the_ID()?>"></div>
                    <div class="left">
                        <?php $images = get_field('galeria');
                        if( $images ): ?>
                            <div class="light-galeria">
                                <?php foreach( $images as $image ): ?>
                                    <div class="galeria-container">
                                        <div class="galeria-container2">
                                            <img src="<?= esc_url($image['url']) ?>" alt="<?= esc_attr($image['alt'])?>">
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div> 
                        <?php endif; ?>
                    </div>
                    <div class="right">
                        <div class="img-container">
                            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="logo">
                        </div>
                        <h3><?= get_the_title() ?></h3>
                        <p><?php the_content(); ?></p>
                        <?php $images = get_field('galeria');?>
                    </div>
                </section>
            </div>
        <?php endif; 
        $index = $index + 1;
        endwhile; 
     endif; ?>
     </div>
</div>
<?php get_footer() ?>
