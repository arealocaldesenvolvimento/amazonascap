<?php
    //Template name: Startup
	get_header(); ?>
<div class="topo lp" style="background-image: url('<?= get_the_post_thumbnail_url() ?>');">
    <section class="al-container lp abertura">
        <h1 class="title"><?= get_the_title() ?></h1>
        <p><?= get_field("chamada-lp") ?></p>
		<?php if(get_field("botoes")){ ?>
			<div class="botoes">
				<?php foreach(get_field("botoes") as $botao){ ?>
					<div id="<?= $botao['formulario'][0] ?>" class="button call-to-form"><?= $botao['texto'] ?></div>
				<?php } ?>
			</div>
		<?php } ?>
    </section>
</div>
<?php if(get_field("video")){ ?>
	<div class="background impar socio" id="socios">
		<section class="al-container quem-somos socios lp">
			<iframe width="100%" height="85.5vh" src="https://www.youtube.com/embed/<?= filter_url_embed(get_field("video")) ?>" title="YouTube video player" frameborder="0"  allowfullscreen></iframe>
		</section>
	</div>
<?php } ?>
<div class="background par lp">
    <section class="al-container lp depoimentos">
        <h1 class="title alt"><strong>depoimentos</strong> de nossos acelerados</h1>
        <?php if( have_rows('depoimentos') ): ?>
            <div class="depoimentos-container">
            <?php while( have_rows('depoimentos') ): the_row();
                $imagem = get_sub_field('imagem');?>
                    <div class="depoimento" title="Clique para ler mais">
                        <img src="<?= get_image_url('quotes.webp')?>" alt="Aspas">
                        <div class="content">
                            <?php the_sub_field('depoimento'); ?>
                        </div>
                        <div class="footer">
                            <div class="left">
                                <h3><?php the_sub_field('nome'); ?></h3>
                                <p><?php the_sub_field('descricao'); ?></p>
                            </div>
                            <div class="right">
                                <div class="img-container">
                                    <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </section>
</div>
<?php if(get_the_content()!=""){ ?>
	<section class="al-container lp proposta">
		<?php the_content(); ?>
	</section>
<?php } ?>
<div class="back2">
    <section class="al-container lp startup">
		<?php if(get_field('titulo_da_ilustracao')){ ?>
	        <div class="title"><?php the_field('titulo_da_ilustracao'); ?></div>
		<?php } ?>
		<?php if(have_rows('keywords')){ ?>
			<div class="keywords">
				<?php while(have_rows('keywords')){ the_row() ?>
					<span><?= get_sub_field("texto") ?></span>
				<?php } ?>
			</div>
		<?php } ?>
        <?php the_field('aceleramos_sua_startup'); ?>
    </section>
</div>
<?php
$current_page = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );
$slug = $current_page->post_name;
?>
<div class="back1" id="<?= $slug ?>">
    <section class="al-container lp formulario">
		<div class="topo">
			<?php if(get_field("contatos")){ ?>
				<?php foreach(get_field("contatos") as $contatoId){?>
					<div class="button-form-lp" id="<?= $contatoId ?>">
						<?= trim(explode(get_the_title(), get_the_title($contatoId))[1]) ?>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
        <div class="form">
			<?php if(get_field("contatos")){ ?>
				<?php foreach(get_field("contatos") as $contatoId){?>
					<?= do_shortcode('[contact-form-7 id="'.$contatoId.'"]'); ?>
				<?php } ?>
			<?php } ?>
        </div>
    </section>
</div>
<?php get_footer(); ?>
