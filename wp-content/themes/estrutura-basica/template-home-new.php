<?php
    //Template name: Home New
    get_header(); ?>
  <section class="banner">
    <?= do_shortcode('[rev_slider alias="home"][/rev_slider]') ?>
  </section>
  <?php if(get_field("solucoes")): ?>
    <section class="solucao">
      <div class="al-container">
        <h1 class="title">O que você quer acelerar?</h1>
        <div class="contain">
          <?php foreach(get_field("solucoes") as $solucoes): ?>
            <a href="<?= $solucoes['url'] ?>">
              <div class="solucao-interna" style="background-image: url(<?= $solucoes['imagem']['url'] ?>);">
                <span>
                  <?= $solucoes['texto'] ?>
                </span>
              </div>
            </a>
          <?php endforeach;?>
        </div>
      </div>
    </section>
  <?php endif; ?>
  <?php if(get_field("video_historia")): ?>
    <section class="historia" style="background-image: url(<?= get_image_url("video-fundo.png") ?>);">
		<i id="close-video" class="fa-solid fa-xmark"></i>
      <div class="al-container">
        <div class="left">
          <?= get_field("historia"); ?>
        </div>
        <div class="right">
          <svg width="90" height="90" viewBox="0 0 90 90" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M40.446 27.5721C39.6054 27.0729 38.6475 26.8051 37.6699 26.7961C36.6922 26.7871 35.7296 27.0371 34.8799 27.5208C34.0302 28.0045 33.3238 28.7046 32.8325 29.5499C32.3411 30.3952 32.0824 31.3555 32.0826 32.3333V57.6668C32.0824 58.6445 32.3411 59.6049 32.8325 60.4502C33.3238 61.2954 34.0302 61.9955 34.8799 62.4793C35.7296 62.963 36.6922 63.213 37.6699 63.204C38.6475 63.195 39.6054 62.9272 40.446 62.428L64.4546 48.1741C65.0053 47.8473 65.4615 47.3829 65.7784 46.8263C66.0953 46.2698 66.262 45.6404 66.262 45C66.262 44.3596 66.0953 43.7303 65.7784 43.1737C65.4615 42.6172 65.0053 42.1528 64.4546 41.826L40.446 27.5721ZM0.710938 45C0.710938 20.5412 20.5415 0.710571 45.0004 0.710571C69.4593 0.710571 89.2899 20.5412 89.2899 45C89.2899 69.4589 69.4593 89.2895 45.0004 89.2895C20.5415 89.2895 0.710938 69.4589 0.710938 45ZM45.0004 6.24676C23.5975 6.24676 6.24712 23.5972 6.24712 45C6.24712 66.4029 23.5975 83.7533 45.0004 83.7533C66.4033 83.7533 83.7537 66.4029 83.7537 45C83.7537 23.5972 66.4033 6.24676 45.0004 6.24676Z" fill="#F9F9F9"/>
          </svg>
          <p>Dê play na<br>nossa história!</p>
        </div>
      </div>
      <div class="video">
        <?= do_shortcode('[video src="'.get_field("video_historia").'" poster="'.get_image_url("video-fundo.png").'"]') ?>
      </div>
    </section>
  <?php endif; ?>
  <section class="ultimas-blog">
    <div class="al-container">
      <h1 class="title">Últimas do Blog</h1>
      <p class="text"><?= get_field("ultimas-blog") ?></p>
      <div class="columns">
        <?php
          $posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'orderby' => 'date',
            'order' => 'DESC',
          ));
          while($posts->have_posts()) : $posts->the_post(); ?>
            <div class="post">
              <a href="<?= get_permalink() ?>">
                <img src="<?= get_the_post_thumbnail_url() ?>" alt="Imagem de destaque">
              </a>
              <div class="text">
                <a href="<?= get_permalink() ?>">
                  <h1><?= get_the_title() ?></h1>
                </a>
                <p><?= get_the_excerpt() ?></p>
              </div>
            </div>
        <?php endwhile; ?>
      </div>
      <div class="btn-contain">
        <a href="<?= get_home_url() ?>/blog" class="button">VEJA TODAS AS POSTAGENS</a>
      </div>
    </div>
  </section>
	<div class="sociais-fixed">
		<a class="sociais" href="<?= get_permalink(get_page_by_path('contato')) ?>">
			<i class="fas fa-comments"></i>
		</a>
	</div>
<?php get_footer(); ?>
