<?php get_header() ?>
    <section class="al-container portfolio" id="cases">
        <div class="left">
            <h1 class="title">Nossos portfólio</h1>
       </div>
        <div class="right">
            <select name="filtro" id="filtro">
                <option value="" disabled="" selected="">FILTRAR POR</option>
                <option value="todos">Todos</option>
                <?php $terms = get_terms([
                    'taxonomy' => 'categoria',
                    'hide_empty' => true,
                    'orderby' => 'id',
                    'order' => 'ASC',
                ]);
                foreach ($terms as $key => $categoria): ?>
                    <option value="<?= $categoria->slug ?>"><?= $categoria->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php
        $cases = new WP_Query(array(
            'post_type' => 'cases',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC'
        )); ?>
        <div id="portfolio">
            <?php if ($cases->have_posts()): ?>
                <?php while($cases->have_posts()) : $cases->the_post(); ?>
                    <div class="case portOpen <?= get_the_terms(get_the_ID(), 'categoria')[0]->slug ?>">
                        <a href="<?= get_the_permalink() ?>">
                            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="logo">
                        </a>
                    </div>
                <?php endwhile; 
            endif; ?>
        </div>
    </section>
<?php get_footer() ?>
