<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.0/swiper-bundle.min.css" integrity="sha512-T9Nrm9JU37BvYFgjYGVYc8EGnpd3nPDz/NY19X6gsNjb0VHorik8KDBljLHvqWdqz9igNqTBvZY4oCJQer4Xtg==" crossorigin="anonymous" />
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/lightslider.min.css"
    >
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-64RN39LMG1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-64RN39LMG1');
    </script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" class="main-header" id="topo">
    <div class="al-container">
        <div class="left">
            <div class="logo-holder">
                <a href="<?= get_home_url(); ?>" id="header-logo">
                    <img src="<?= get_image_url('logo.png') ?>" alt="AmazonasCap">
                </a>
            </div>
        </div>
        <?php if (!empty(get_field('texto_botao_destaque', 'options'))):?>
        <div class="destaque">
            <a href="<?= get_field('link_botao_destaque', 'options') ?>" class="button"><?= get_field('texto_botao_destaque', 'options') ?></a>
        </div>
        <?php endif; ?>
        <div class="right">
            <!-- <form id="buscaheader" class="busca" action="<?= get_page_link(get_page_by_path('blog')) ?>" method="get">   
                    <input type="text" placeholder="Pesquisar no blog" name="pesquisa"/>   
                    <button type="submit" class="button"><i class="fas fa-search"></i></button>
            </form> -->
            <div class="linguagem"></div>
            <div id="menu-bars">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 384.97 384.97" style="enable-background:new 0 0 512 512" xml:space="preserve" class="normal" id="menu-bars1">
                    <g>
                        <g xmlns="http://www.w3.org/2000/svg">
                            <g id="Menu_1_">
                                <path d="M12.03,120.303h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03    c-6.641,0-12.03,5.39-12.03,12.03C0,114.913,5.39,120.303,12.03,120.303z" fill="#d37131" data-original="#000000" style=""/>
                                <path d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03    S379.58,180.455,372.939,180.455z" fill="#d37131" data-original="#000000" style=""/>
                                <path d="M372.939,264.667H132.333c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h240.606    c6.641,0,12.03-5.39,12.03-12.03C384.97,270.056,379.58,264.667,372.939,264.667z" fill="#d37131" data-original="#000000" style=""/>
                            </g>
                        </g>
                    </g>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 384.97 384.97" style="enable-background:new 0 0 512 512" xml:space="preserve" class="hover" id="menu-bars2">
                    <g>
                        <g xmlns="http://www.w3.org/2000/svg">
                            <g id="Menu_1_">
                                <path d="M12.03,120.303h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03    c-6.641,0-12.03,5.39-12.03,12.03C0,114.913,5.39,120.303,12.03,120.303z" fill="#23694d" data-original="#000000" style="" class=""/>
                                <path d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03    S379.58,180.455,372.939,180.455z" fill="#23694d" data-original="#000000" style="" class=""/>
                                <path d="M372.939,264.667H132.333c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h240.606    c6.641,0,12.03-5.39,12.03-12.03C384.97,270.056,379.58,264.667,372.939,264.667z" fill="#23694d" data-original="#000000" style="" class=""/>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
            <div class="menu-occ conditionalClose" id="menu-occ">
                <div class="close" id="close">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512 512" xml:space="preserve" class="normal">
                        <g>
                            <g xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z" fill="#d57831" data-original="#000000" style="" class=""/>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512 512" xml:space="preserve" class="hover">
                        <g>
                            <g xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z" fill="#23694d" data-original="#000000" style="" class=""/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="institucional">
                    <?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
                </div>
                <div class="newsletter">
                    <a class="button news">Assine nossa newsletter</a>
                </div>
                <div class="newsletter-occ">
                    <?= do_shortcode('[newsletter_form]'); ?>
                </div>
                <div class="social">
                    <a class="sociais" target="blank" href="https://twitter.com/amazonascap">
                    <i class="fab fa-twitter"></i></a>
                    <a class="sociais" target="blank" href="https://www.linkedin.com/company/amazonascap-aceleradora">
                    <i class="fab fa-linkedin-in"></i></a>
                    <a class="sociais" target="blank" href="https://www.instagram.com/amazonascap/">
                    <i class="fab fa-instagram"></i></a>
                    <a class="sociais" target="blank" href="https://www.youtube.com/channel/UCdAvBpHbAVYEBo7a1kyOV-g/videos">
                    <i class="fab fa-youtube"></i></a>
                </div>
                <div class="overlay" id="overlay"></div>
            </div>
        </div>
    </div>
    </header>
    <div class="floating">
        <a class="sociais" href="<?= get_site_url() ?>/contato">
        <i class="fas fa-comments"></i>
        </a>
    </div>
    <!-- Wrapper -->
    <div id="wrapper">
