    </div>

    <!-- Footer -->
    <footer class="main-footer">
    <div class="left">
        <div class="small-container">
            <div class="logo-holder">
                <a href="<?= get_home_url(); ?>" id="header-logo">
                    <img src="<?= get_image_url('logo-footer.png') ?>" alt="AmazonasCap">
                </a>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="small-container">
            <div class="social">
                <a class="sociais" target="blank" href="https://twitter.com/amazonascap">
                <i class="fab fa-twitter"></i></a>
                <a class="sociais" target="blank" href="https://www.linkedin.com/company/amazonascap-aceleradora">
                <i class="fab fa-linkedin-in"></i></a>
                <a class="sociais" target="blank" href="https://www.instagram.com/amazonascap/">
                <i class="fab fa-instagram"></i></a>
                <a class="sociais" target="blank" href="https://www.youtube.com/channel/UCdAvBpHbAVYEBo7a1kyOV-g/videos">
                <i class="fab fa-youtube"></i></a>
            </div>
            <div class="contato">
                <!-- <h2>Contato</h2> -->
                <a href="https://goo.gl/maps/SXjrp75NXgbCprE28" target="_blank">
                Rua Estados Unidos,<br>
                411 - Jardim América<br>
                São Paulo - SP<br>
                01427-000</a>
                <a href="mailto:contato@amazonascap.com.br">contato@amazonascap.com.br</a>
            </div>
            <p>©2021 TODOS OS<br>
            DIREITOS RESERVADOS.</p>
            <div class="area-local">
                <a href="http://www.arealocal.com.br" target="_blank" title="Área Local">Desenvolvido por: Área Local</a>
            </div>
        </div>
    </div>
    </footer>    
    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script src="https://kit.fontawesome.com/4800576786.js" crossorigin="anonymous"></script>
    <script
        type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"
    ></script>
    <script
        async
        type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/public/js/app.js"
    ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.0/swiper-bundle.min.js" integrity="sha512-VfcksjYXPZW36rsAGxeRGdB0Kp/htJF9jY5nlofHtRtswIB+scY9sbCJ5FdpdqceRRkpFfHZ3a9AHuoL4zjG5Q==" crossorigin="anonymous"></script>
    <?php wp_footer(); ?>
</body>
</html>
