<?php

flush_rewrite_rules();

function projetos()
{
    register_post_type('projetos', [
        'labels' => [
            'name' => _x('Projetos', 'post type general name'),
            'singular_name' => _x('Projetos', 'post type singular name'),
            'add_new' => __('Adicionar Projeto'),
            'add_new_item' => __('Adicionar Projeto'),
            'edit_item' => __('Editar Projetos'),
            'new_item' => __('Novo Projeto'),
            'view_item' => __('Ver Projetos'),
            'not_found' =>  __('Nenhum Projeto encontrado'),
            'not_found_in_trash' => __('Nenhum Projeto encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Projetos'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-images-alt',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'projetos');

function socios()
{
    register_post_type('socios', [
        'labels' => [
            'name' => _x('Sócios', 'post type general name'),
            'singular_name' => _x('Sócios', 'post type singular name'),
            'add_new' => __('Adicionar Sócio'),
            'add_new_item' => __('Adicionar Sócio'),
            'edit_item' => __('Editar Sócios'),
            'new_item' => __('Novo Sócio'),
            'view_item' => __('Ver Sócios'),
            'not_found' =>  __('Nenhum Sócio encontrado'),
            'not_found_in_trash' => __('Nenhum Sócio encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Sócios'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-businessperson',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'socios');

function mentores()
{
    register_post_type('mentores', [
        'labels' => [
            'name' => _x('Mentores', 'post type general name'),
            'singular_name' => _x('Mentores', 'post type singular name'),
            'add_new' => __('Adicionar Mentor'),
            'add_new_item' => __('Adicionar Mentor'),
            'edit_item' => __('Editar Mentores'),
            'new_item' => __('Novo Mentor'),
            'view_item' => __('Ver Mentores'),
            'not_found' =>  __('Nenhum Mentor encontrado'),
            'not_found_in_trash' => __('Nenhum Mentor encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Mentores'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-buddicons-buddypress-logo',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'mentores');

function parceiros()
{
    register_post_type('parceiros', [
        'labels' => [
            'name' => _x('Parceiros', 'post type general name'),
            'singular_name' => _x('Parceiros', 'post type singular name'),
            'add_new' => __('Adicionar Parceiro'),
            'add_new_item' => __('Adicionar Parceiro'),
            'edit_item' => __('Editar Parceiros'),
            'new_item' => __('Novo Parceiro'),
            'view_item' => __('Ver Parceiros'),
            'not_found' =>  __('Nenhum Parceiro encontrado'),
            'not_found_in_trash' => __('Nenhum Parceiro encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Parceiros'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-universal-access',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'parceiros');

function timeline()
{
    register_post_type('timeline', [
        'labels' => [
            'name' => _x('Timeline', 'post type general name'),
            'singular_name' => _x('Timeline', 'post type singular name'),
            'add_new' => __('Adicionar item'),
            'add_new_item' => __('Adicionar item'),
            'edit_item' => __('Editar itens'),
            'new_item' => __('Novo item'),
            'view_item' => __('Ver itens'),
            'not_found' =>  __('Nenhum item encontrado'),
            'not_found_in_trash' => __('Nenhum item encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Timeline'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-clock',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'timeline');

function depoimentos()
{
    register_post_type('depoimentos', [
        'labels' => [
            'name' => _x('Depoimentos', 'post type general name'),
            'singular_name' => _x('Depoimentos', 'post type singular name'),
            'add_new' => __('Adicionar Depoimento'),
            'add_new_item' => __('Adicionar Depoimento'),
            'edit_item' => __('Editar Depoimentos'),
            'new_item' => __('Novo Depoimento'),
            'view_item' => __('Ver Depoimentos'),
            'not_found' =>  __('Nenhum Depoimento encontrado'),
            'not_found_in_trash' => __('Nenhum Depoimento encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Depoimentos'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-universal-access',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'depoimentos');

function kapoks()
{
    register_post_type('kapoks', [
        'labels' => [
            'name' => _x('Kapoks', 'post type general name'),
            'singular_name' => _x('Kapoks', 'post type singular name'),
            'add_new' => __('Adicionar Kapok'),
            'add_new_item' => __('Adicionar Kapok'),
            'edit_item' => __('Editar Kapoks'),
            'new_item' => __('Novo Kapok'),
            'view_item' => __('Ver Kapoks'),
            'not_found' =>  __('Nenhum Kapok encontrado'),
            'not_found_in_trash' => __('Nenhum Kapok encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Kapoks'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-format-image',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'kapoks');

function startups()
{
    register_post_type('startups', [
        'labels' => [
            'name' => _x('Startups', 'post type general name'),
            'singular_name' => _x('Startups', 'post type singular name'),
            'add_new' => __('Adicionar Startup'),
            'add_new_item' => __('Adicionar Startup'),
            'edit_item' => __('Editar Startups'),
            'new_item' => __('Novo Startup'),
            'view_item' => __('Ver Startups'),
            'not_found' =>  __('Nenhum Startup encontrado'),
            'not_found_in_trash' => __('Nenhum Startup encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Startups'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-media-document',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);

    register_taxonomy(
        "categoria",
        "startups",
        array(
            "label" => "Categorias",
            "singular_label" => "Categoria",
            "rewrite" => true,
            "hierarchical" => true
        )
    );
}
add_action('init', 'startups');