<?php get_header() ?>
<div class="back1">
    <section class="al-container quem-somos valores" id="valores">
        <h1 class="title"><?= get_the_title() ?></h1>
        <?php the_content(); ?>
        <div class="left">
            <h1 class="title alt">Propósito e<br>valores</h1>
        </div>
        <div class="right">
            <p><?= get_field('proposito_e_valores') ?></p>
        </div>
        <div class="cards-valores">
            <div class="card comprometimento">
                <h1>Comprometimento</h1>
                <p><?= get_field('comprometimento') ?></p>
            </div>
            <div class="card responsabilidade">
                <h1>responsabilidade</h1>
                <p><?= get_field('responsabilidade') ?></p>
            </div>
            <div class="card confianca">
                <h1>confiança</h1>
                <p><?= get_field('confianca') ?></p>
            </div>
            <div class="card lideranca">
                <h1>liderança</h1>
                <p><?= get_field('lideranca') ?></p>
            </div>
            <div class="card sucesso">
                <h1>sucesso</h1>
                <p><?= get_field('sucesso') ?></p>
            </div>
            <div class="card empreendedorismo">
                <h1>empreendedorismo</h1>
                <p><?= get_field('empreendedorismo') ?></p>
            </div>
        </div>
    </section>
</div>
<div class="background impar socio" id="socios">
    <section class="al-container quem-somos socios">
        <h1>Sócios</h1>
        <div class="socios-container">
            <?php
            $socios = new WP_Query(array(
                'post_type' => 'socios',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => 'ASC'
            )); 
            if ($socios->have_posts()): 
                while($socios->have_posts()) : $socios->the_post(); ?>
                    <div class="socio">
                        <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                        <div class="social">
                            <h3><?= get_the_title() ?></h3>
                            <a class="sociais" target="blank" href="<?= get_field('linkedin') ?>">
                            <i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>
                <?php endwhile; 
                wp_reset_postdata();
            endif; ?>
        </div>
    </section>
</div>
<section class="al-container quem-somos associados" id="associados">
    <h1 class="title">Associados</h1>
    <div class="associados-container">
        <?php if( have_rows('associados') ): ?>
            <?php while( have_rows('associados') ): the_row(); ?>
                <div class="associado">
                    <!-- <div class="left"> -->
                        <div class="img-container">
                            <a class="sociais" target="blank" href="<?= get_sub_field('linkedin') ?>">
                            <i class="fab fa-linkedin-in"></i></a>
                            <img src="<?= get_sub_field('imagem')['url'] ?>" alt="<?= get_sub_field('nome') ?>">
                        </div>  
                    <!-- </div> -->
                    <!-- <div class="right"> -->
                        <div class="content">
                            <h3><?= str_replace_first(" ", "<br>", get_sub_field('nome')) ?></h3>
                            <p><?= get_sub_field('cargo') ?></p>
                        </div>
                    <!-- </div> -->
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>
<section class="al-container quem-somos mentores" id="mentores">
    <header>
        <h1 class="title alt">Mentores</h1>  
        <div class="prev"><i class="fas fa-chevron-left"></i></div>
        <div class="next"><i class="fas fa-chevron-right"></i></div>
    </header>
    <?php
    $mentores = new WP_Query(array(
        'post_type' => 'mentores',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'rand',
        'order' => 'ASC'
    )); 
    if ($mentores->have_posts()):  ?>
        <div class="swiper-container">
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-wrapper">
                <?php while($mentores->have_posts()) : $mentores->the_post(); ?>
                    <div class="mentor swiper-slide">
                        <div class="img-container">
                            <a class="sociais" target="blank" href="<?= get_field('linkedin') ?>">
                            <i class="fab fa-linkedin-in"></i></a>
                            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                        </div>  
                        <h3><?= get_the_title() ?></h3>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>
</section>
<div class="cinza">
    <section class="al-container quem-somos parceiros"  id="parceiros">
        <h1 class="title">Parceiros</h1>
        <div class="parceiros-container">
            <?php
            $parceiros = new WP_Query(array(
                'post_type' => 'parceiros',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => 'ASC'
            )); 
            if ($parceiros->have_posts()): 
                while($parceiros->have_posts()) : $parceiros->the_post(); ?>
                    <div class="parceiro">
                        <?php if (!empty(get_field('link_do_parceiro'))): ?>
                            <a target="blank" rel="noopener noreferrer" alt="<?= get_the_title() ?>" href="<?= get_field('link_do_parceiro') ?>">
                                <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                            </a>
                        <?php else: ?>
                            <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                        <?php endif; ?>
                    </div>
                <?php endwhile; 
            endif; ?>
        </div>
    </section>
</div>
<section class="al-container quem-somos timeline" id="timeline">
    <header>
        <h1 class="title alt">Timeline</h1>
        <div class="prev"><i class="fas fa-chevron-left"></i></div>
        <div class="next"><i class="fas fa-chevron-right"></i></div>
    </header>
    <div class="timeline-container swiper-container">
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-wrapper">
            <?php
            $timeline = new WP_Query(array(
                'post_type' => 'timeline',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => 'ASC'
            )); 
            if ($timeline->have_posts()): 
                while($timeline->have_posts()) : $timeline->the_post(); ?>
                    <div class="timeline-item swiper-slide">
                        <header>
                            <div class="img-container">
                                <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                            </div>
                        </header>
                        <h3><?= get_field('ano') ?></h3>
                        <p><?php the_content(); ?></p>
                    </div>
                <?php endwhile; 
            endif; 
            wp_reset_postdata();?>
        </div>
    </div>
</section>
<div class="background par">
    <section class="al-container quem-somos slider" id="kapoks">
        <div class="left">
            <h1>kapoks</h1>
            <p><?= get_field('kapoks') ?></p>
        </div>
        <div class="right">
            <div class="light-galeria">
                <?php
                $kapoks = new WP_Query(array(
                    'post_type' => 'kapoks',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'order' => 'ASC'
                )); 
                if ($kapoks->have_posts()): 
                    while($kapoks->have_posts()) : $kapoks->the_post(); ?>
                        <div class="galeria-container">
                                <div class="galeria-container2">
                                    <div class="img-container">
                                        <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                                    </div>
                                    <div class="content">
                                        <h3><?= get_the_title() ?></h3>
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                    <?php endwhile; 
                endif; ?>
            </div>
        </div> 
    </section>
</div>
<?php get_footer() ?>
