<?php get_header() ?>
<section class="al-container blog listagem">
    <div class="left">
        <h1 class="title">últimas notícias</h1>
    </div>
    <div class="right">
        <form id="buscaheader" class="busca" action="<?= get_page_link(get_page_by_path('blog')) ?>" method="get">   
                <select name="filtro" id="filtro">
                    <option value="" disabled="" selected="">FILTRAR POR</option>
                    <?php $terms = get_terms([
                        'taxonomy' => 'category',
                        'hide_empty' => true,
                        'orderby' => 'title',
                        'order' => 'ASC',
                    ]);
                    foreach ($terms as $key => $categoria): ?>
                        <option value="<?= $categoria->term_id ?>" <?= $categoria->term_id == $_GET['filtro'] ? 'selected' : '' ?>><?= $categoria->name ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="text" placeholder="Pesquisar no blog" <?= empty($_GET['pesquisa']) ? '' : 'value="'.$_GET['pesquisa'].'"' ?> name="pesquisa"/>   
                <button type="submit" class="button"><i class="fas fa-search"></i></button>
        </form> 
    </div>
    <div class="noticias">
        <?php
        $pesquisa = empty($_GET['pesquisa']) ? "" : $_GET['pesquisa'];
        $filtro = '';
        if(!empty($_GET['filtro'])){
            $filtro = array (
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $_GET['filtro'],
            );
        }else if (!empty(get_queried_object()->term_id)) {
            $filtro = array (
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => get_queried_object()->term_id,
            );
        }
        $blog = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'posts_join' => true,
            'tax_query' => array($filtro),
            'orderby' => 'date',
            'post_like' => $pesquisa,
            'order' => 'desc'
        ));
        if (!$blog->have_posts())
            echo '<p style="text-align: center;">Nenhum Resultado Correspondente Encontrado</p>';
        while($blog->have_posts()) : $blog->the_post(); ?>
        <article class="clearfix artigo portOpen" id="id-<?php the_ID(); ?>">
            <a href="<?= get_the_permalink() ?>" title="<?= get_the_title() ?>">
                <img src="<?=  get_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title() ?>"/>
            </a>
            <div class="content">
                <header>
                    <h2><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h2>
                    <?php the_excerpt(); ?>
                </header>
                <span class="leia-mais"><a href="<?= get_permalink() ?>">Continue Lendo <i class="fas fa-chevron-right"></i></a></span>
            </div>
        </article>
        <?php endwhile; ?>
    </div>
</section>
<?php get_footer() ?>

