<?php get_header() ?>
<div class="al-container linha">
    <section class="al-container blog single">
        <?php  if ( function_exists('yoast_breadcrumb') ){
            yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }?>
        <h1 class="title alt"><?= get_the_title()?></h1>
        <span class="data"><?= get_the_date() ?></span>
        <?php while(have_posts()) : the_post(); ?>
            <article class="clearfix" id="id-<?php the_ID(); ?>">
                <div class="img-container">
                    <img src="<?=  get_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?= get_the_title() ?>"/>
                </div>
                <div class="content"> 
                    <?php the_content(); ?>
                </div>
                <div class="share hidden">
                    <?= do_shortcode('[addtoany]'); ?>
                </div>
            </article>
        <?php endwhile; ?>
    </section>
</div>
<section class="al-container blog single relacionadas">
    
    <h1 class="title">notícias relacionadas</h1>
    <div class="noticias">
        <?php
        $object = get_the_category();
        $categoria = array (
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $object[0]->term_id,
        );
        $blog = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'post__not_in' => array(get_the_ID()), 
            'posts_join' => true,
            'tax_query' => array($categoria),
            'orderby' => 'date',
            'order' => 'desc'
        ));
        if (!$blog->have_posts()){
            $blog = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'post__not_in' => array(get_the_ID()), 
                'posts_join' => true,
                'orderby' => 'date',
                'order' => 'desc'
            ));
        }
        if (!$blog->have_posts())
            echo '<p style="text-align: center;">Nenhum Resultado Correspondente Encontrado</p>';
        while($blog->have_posts()) : $blog->the_post(); ?>
        <article class="clearfix portOpen" id="id-<?php the_ID(); ?>">
            <a href="<?= get_the_permalink() ?>" title="<?= get_the_title() ?>">
                <img src="<?=  get_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title() ?>"/>
            </a>
            <div class="content">
                <header>
                    <h2><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h2>
                    <?php the_excerpt(); ?>
                </header>
                <span class="leia-mais"><a href="<?= get_permalink() ?>">Continue Lendo <i class="fas fa-chevron-right"></i></a></span>
            </div>
        </article>
        <?php endwhile; ?>
    </div>
</section>

<?php get_footer() ?>
