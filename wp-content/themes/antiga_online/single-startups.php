<?php get_header() ?>
<div class="back1">
    <section class="al-container single case">
        <h1 class="title">Nossos portfólio</h1>
        <div class="left">
            <a href="<?= get_field('link') ?>" target="_blank" rel="noopener noreferrer">
                <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="logo">
            </a>
            <?php the_content(); ?>
            <p><a href="<?= get_field('link') ?>" target="_blank" rel="noopener noreferrer"><?= get_field('link') ?></a></p>
            <?php if (get_field('plano_de_negocio') == true): ?>
                <a href="<?= get_site_url() ?>/declaracao-do-investidor" class="button">requisição de plano de negócio</a>
            <?php endif; ?>
        
        </div>
        <div class="right">
        <?php if (!empty(get_field('video'))): ?>
            <iframe src="<?='https://www.youtube.com/embed/'.end(explode("=", end(explode("/", get_field('video')))))?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <?php elseif (!empty(get_field('imagem'))): ?>
            <img src="<?= get_field('imagem')['url'] ?>" alt="<?= get_the_title() ?>">
        <?php endif; ?>
        </div>
    </section>
</div>
<?php get_footer() ?>
