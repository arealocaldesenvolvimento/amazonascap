<?php

/* Helpers
========================================================================== */
// include_once get_theme_file_path('app/helpers/...')

/* App
========================================================================== */
include_once get_theme_file_path('app/config.php');
include_once get_theme_file_path('app/scripts/post-types.php');

/* Actions & Filters
========================================================================== */
add_action('show_admin_bar', '__return_false');
add_filter('widget_text', 'do_shortcode');
add_filter('retrieve_password_message', 'reset_password_message', null, 2);
remove_action('wp_head', 'wp_generator');

/* Theme functions
========================================================================== */

function wp_mail_return_texthtml(): string
{
    return 'text/html';
}
add_filter('wp_mail_content_type', 'wp_mail_return_texthtml');

/**
 * Returns the full path of the image.
 *
 * @param string $file nome do arquivo
 *
 * @return string caminho completo do arquivo
 */
function get_image_url(string $file = ''): string
{
    return get_template_directory_uri().'/assets/images/'.$file;
}

/**
 * Print the full path of the image.
 *
 * @param string $file nome do arquivo
 */
function image_url(string $file = '')
{
    echo get_image_url($file);
}

/**
 * Print the image of the logo, if it is the home place a wrap of h1.
 */
function get_logo(string $img = 'logo.png')
{
    $blogName = get_bloginfo('name');

    $tag = '<a href="%s" title="%s - %s" id="header-logo"><img src="%s" alt="%s"></a>';
    $tag = ($tag) ? "<h1 id='wrap-logo'>{$tag}</h1>" : '';
    $tag = sprintf($tag, home_url('/'), $blogName, get_bloginfo('description'), get_image_url($img), $blogName);

    echo $tag;
}

/**
 * Change text at the bottom of the panel.
 */
function change_panel_footer_text()
{
    echo '&copy; <a href="http://www.arealocal.com.br/" target="_blank">&Aacute;rea Local</a> - Sites - E-commerce';
}
add_filter('admin_footer_text', 'change_panel_footer_text');

/**
 * Change the login form logo.
 */
function change_login_form_logo()
{
    echo '<style>.login h1 a{background-image:url('.get_image_url('logo-login-form.png').')!important;}</style>';
}
add_action('login_enqueue_scripts', 'change_login_form_logo');

/**
 * Change the login form logo url.
 */
function login_form_logo_url()
{
    return get_home_url();
}
add_filter('login_headerurl', 'login_form_logo_url');

/**
 * Standardizes login error message, so as not to show when the user exists.
 */
function wrong_login()
{
    return '<b>ERRO</b>: Usuário ou senha incorretos.';
}
add_filter('login_errors', 'wrong_login');

/**
 * Change title of the login form logo.
 */
function login_form_logo_url_title()
{
    return get_bloginfo('name').' - Desenvolvido por Área Local';
}
add_filter('login_headertext', 'login_form_logo_url_title');

/**
 * Adds main navigation, html5 support and post thumbnail.
 */
function al_setup()
{
    register_nav_menus(['principal' => 'Navegação Principal']);
    add_theme_support('post-thumbnails');
    add_theme_support('html5', ['comment-list', 'comment-form', 'search-form', 'gallery', 'caption']);
}
add_action('after_setup_theme', 'al_setup');

/**
 * Add home in the menu.
 */
function show_home_menu(array $args)
{
    $args['show_home'] = true;

    return $args;
}
add_filter('wp_page_menu_args', 'show_home_menu');

/**
 * Records widget areas.
 */
function add_widget_areas()
{
    register_sidebar([
        'name' => 'Área de Widget Primária',
        'id' => 'area-widget-primaria',
        'description' => 'Área de Widget Primária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => 'Área de Widget Secundária',
        'id' => 'area-widget-secundaria',
        'description' => 'Área de Widget Secundária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ]);
}
add_action('widgets_init', 'add_widget_areas');

/**
 * Disables wordpress emojis.
 */
function disable_wp_emojicons()
{
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
}
add_action('init', 'disable_wp_emojicons');

/**
 * Corrects accent file upload bug.
 *
 * @param string $filename string nome do arquivo original
 *
 * @return string nome do arquivo higienizado
 */
function sanitize_filename(string $filename)
{
    $ext = explode('.', $filename);
    $ext = end($ext);
    $sanitized = preg_replace('/[^a-zA-Z0-9-_.]/', '', substr($filename, 0, -(strlen($ext) + 1)));
    $sanitized = str_replace('.', '-', $sanitized);

    if (function_exists('sanitize_title')) {
        $sanitized = sanitize_title($sanitized);
    }

    return strtolower($sanitized.'.'.$ext);
}
add_filter('sanitize_file_name', 'sanitize_filename', 10);

/**
 * Limit excerpt to a number of characters.
 */
function custom_short_excerpt(string $excerpt, int $length = 200): string
{
    $text = substr($excerpt, 0, $length);

    if (strlen($excerpt) > $length) {
        $text .= ' [...]';
    }

    return $text;
}
add_filter('the_excerpt', 'custom_short_excerpt');

/**
 * Change excerpt tag.
 */
function change_excerpt_more()
{
    return '<a title="'.get_the_title().'" href="'.get_permalink().'" class="more-link">Ler mais</a>';
}
add_filter('excerpt_more', 'change_excerpt_more');

/**
 * Add admin.js and admin.css script to the admin screen.
 */
function adminScriptsStyles()
{
    wp_enqueue_style('admin-styles', get_bloginfo('template_url').'/assets/css/admin.css');
    wp_enqueue_script('admin-functions', get_bloginfo('template_url').'/assets/js/admin.js');
}
add_action('admin_enqueue_scripts', 'adminScriptsStyles');

/**
 * Render Área Local help panel.
 */
function custom_dashboard_help()
{
    echo '
		<p>
			Bem-vindo ao tema Área Local! precisa de ajuda?</br> Contate o suporte
			<a target="_blank" href="https://suporte-arealocal.tomticket.com/">aqui!</a>
		</p>
		<h2>Contato</h2>
		<p>Telefone/Whastapp: <a target="_blank" href="https://wa.me/554735219850"><b>(47) 3521-9850</b></a></p>
		<p>E-mail:
			<a target="_blank" href="mailto:contato@arealocal.com.br"><b>contato@arealocal.com.br</b></a>
		</p>
	';
}

/**
 * Enables service area on the Wordpress dashboard.
 */
function my_custom_dashboard_widgets()
{
    wp_add_dashboard_widget('atendimento-arealocal', 'Atendimento Área Local', 'custom_dashboard_help');
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

/**
 * Brings the thumbnail, post image or standard image.
 *
 * @param mixed $post_id
 * @param mixed $size
 */
function get_thumbnail_url($post_id, $size)
{
    if (!isset($post_id)) {
        $post_id = get_the_ID();
    }
    if (has_post_thumbnail($post_id)) {
        $post_thumbnail_url = get_the_post_thumbnail_url('', $size);
    } else {
        $post_thumbnail_url = get_image_url('default.png');
    }

    return $post_thumbnail_url;
}

/**
 * Render pagination links.
 */
function paginationLinks(WP_Query $query, string $url = '')
{
    $big = 999999999;
    $url = empty($url) ? str_replace($big, '%#%', esc_url(get_pagenum_link($big))) : $url.'page/%#%/';

    $imgUrl = get_image_url('flecha-paginacao.svg');

    if ($query->max_num_pages > 1) {
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links([
            'base' => $url,
            'show_all' => false,
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $query->max_num_pages,
            'prev_text' => "<img class='arrow-left' src='{$imgUrl}'>",
            'next_text' => "<img class='arrow-right' src='{$imgUrl}'>",
        ]);
    }
}

/**
 * Adequate pagination for single-post and archive-post.
 */
function custom_posts_per_page(WP_Query $query)
{
    global $pagenow;

    if ($query->is_archive('post') && 'edit.php' !== $pagenow) {
        set_query_var('posts_per_page', 1);
    }
}
add_action('pre_get_posts', 'custom_posts_per_page');

/**
 * Change some archive page title.
 */
function customArchiveTitles(string $title)
{
    if (is_post_type_archive('some_archive')) {
        return 'Some Archive';
    }

    return $title;
}
add_filter('wp_title', 'customArchiveTitles');
add_filter('get_the_archive_title', 'customArchiveTitles');

/**
 * Custom actions before template rendering.
 */
function preTemplateAction(string $template)
{
    return $template;
}
add_filter('template_include', 'preTemplateAction');

// Change admin panel color scheme
wp_admin_css_color(
    'area-structure-4',
    __('Área Structure 4'),
    get_bloginfo('template_url').'/assets/css/al-admin-color-scheme.min.css',
    [
        '#222',
        '#e0e047',
        '#00244c',
        '#19539a',
    ],
    [
        'base' => '#e5f8ff',
        'focus' => '#fff',
        'current' => '#fff',
    ]
);

/**
 * Set "area-structure-4" as default color scheme.
 */
function setDefaultAdminColor(int $userId)
{
    wp_update_user([
        'ID' => $userId,
        'admin_color' => 'area-structure-4',
    ]);
}
add_action('user_register', 'setDefaultAdminColor');

/**
 * Redirecionamento costumizado, conforme categoria ou tax
//  */
// function template_redirect($template) {
//     if(is_archive('projetos')) {
//         $template = get_query_template('page-nossos-projetos');
//     }

//    return $template;
// }
// add_filter('template_include', 'template_redirect');


/* WP REST
========================================================================== */
include_once get_theme_file_path('app/routes.php');

/* Menu Dinamico para os itens #post-type */
add_filter( 'wp_get_nav_menu_items', 'archive_menu_filter', 10, 3 );
function archive_menu_filter( $items, $menu, $args ) {
  $menuTags = array(
    "#projetos",
    );

  $menu_order = count($items); /* Offset menu order */

  $child_items = array();
  if (!is_admin()){
      foreach ( $items as &$item ) {

        if (!in_array($item->url, $menuTags)) continue;
        $postType = str_replace("#", "", $item->url);

        $item->url = get_site_url()."/nossos-projetos/";
        /* retrieve all children */
        $projetos = new WP_Query(array(
            'post_type' => 'projetos',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC'
        )); 
        while($projetos->have_posts()) : $projetos->the_post(); 
            /* hydrate with menu-specific information */
            $item_menu = [];

            global $post; 

            $post->menu_item_parent = $item->ID;
            $post->post_type = 'nav_menu_item';
            $post->object = 'custom';
            $post->type = 'custom';
            $post->menu_order = ++$menu_order;
            $post->url = get_site_url()."/nossos-projetos/"."#id-".get_the_ID();
            $post->title = get_the_title();
            /* add as a child */
            $child_items[] = $post;
        endwhile;
        wp_reset_postdata();
      }
  }
  return array_merge( $items, $child_items );
}

function prepare_string($str){
    $str = strtolower($str);
    $str = str_replace(".", "--", $str);
    $str = str_replace(",", "--", $str);
    $str = str_replace(";", "--", $str);
    $str = str_replace("\"", "--", $str);
    $str = str_replace("select", "--", $str);
    $str = str_replace("drop", "--", $str);
    $str = str_replace("update", "--", $str);
    $str = str_replace("show", "--", $str);

    return preg_replace(array("/(á|à|ã|â|ä)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ç)/"),explode(" ","a e i o u c"),$str);
}

//Pesquesa por titulo, taxonomia ou meta_value do post
function posts_like_where($where, $wp_query) {
    global $wpdb;
    if (($post_like = prepare_string($wp_query->get('post_like'))) && $wp_query->get('posts_join') && empty($wp_query->get('tax_query')[0]) && empty($wp_query->get('category_name'))) {
        $where .= ' AND ('.$wpdb->posts.'.post_title LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\'';
        $where .= ' OR mt99.meta_value LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\'';
        $where .= ' OR '.$wpdb->terms.'.name LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\')';
    }elseif($post_like = prepare_string($wp_query->get('post_like'))){
        $where .= ' AND ('.$wpdb->posts.'.post_title LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\'';
        $where .= ' OR mt99.meta_value LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\')';
    }
    return $where;
}
add_filter( 'posts_where', 'posts_like_where', 10, 2 );

function join_tables($join, $wp_query) {
    global $wpdb;
    if($wp_query->get('posts_join') && empty($wp_query->get('tax_query')[0]) && empty($wp_query->get('category_name'))){
        $join .=' JOIN '.$wpdb->postmeta.' AS mt99 ON ('.$wpdb->posts.'.ID = mt99.post_id) ';
        $join .=' JOIN '.$wpdb->term_relationships.' ON '.$wpdb->posts.'.ID = '.$wpdb->term_relationships.'.object_id';
        $join .=' JOIN '.$wpdb->term_taxonomy.' ON '.$wpdb->term_relationships.'.term_taxonomy_id = '.$wpdb->term_taxonomy.'.term_taxonomy_id ';
        $join .=' JOIN '.$wpdb->terms.' ON '.$wpdb->term_taxonomy.'.term_id = '.$wpdb->terms.'.term_id ';
    }elseif($wp_query->get('posts_join')){
        $join .=' JOIN '.$wpdb->postmeta.' AS mt99 ON ('.$wpdb->posts.'.ID = mt99.post_id) ';
    }
    return $join;
}
add_filter('posts_join', 'join_tables', 10, 2 );


function groupby_tables($groupby) {
    global $wpdb;
    $groupby = "{$wpdb->posts}.ID";
    return $groupby;
}
add_filter( 'posts_groupby', 'groupby_tables' );

function title_like_posts_where( $where, $wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $post_title_like ) ) . '%\'';
    }
    return $where;
}
add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );

function str_replace_first($from, $to, $content)
{
    $from = '/'.preg_quote($from, '/').'/';

    return preg_replace($from, $to, $content, 1);
}

add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_home() || is_singular( 'post' ) || is_archive() ) {
        $breadcrumb[] = array(
            'url' => get_permalink(get_page_by_title( 'Blog' )),
            'text' => 'Blog',
        );

        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}

if(function_exists('acf_add_options_page')) {
	acf_add_options_page();
}