<?php get_header(); ?>
<div class="topo lp">
    <section class="al-container lp abertura">
        <h1 class="title">Aceleração Early Stage</h1>
        <p>Mentorias <strong>100% online</strong> com instrutores e mentores experientes</p>
        <a href="#form" class="button">seleção aberta. <strong>increva-se</strong></a>
    </section>
</div>
<div class="background impar socio" id="socios">
    <section class="al-container quem-somos socios lp">
        <h1 class="title">ACELERE SUA STARTUP <strong>COM QUE ENTENDE DO ASSUNTO</strong></h1>
        <div class="keywords">
            <span>conhecimento</span>
            <span>negócios</span>
            <span>networking</span>
        </div>
        <div class="socios-container">
            <?php
            $socios = new WP_Query(array(
                'post_type' => 'socios',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => 'ASC'
            )); 
            if ($socios->have_posts()): 
                while($socios->have_posts()) : $socios->the_post(); ?>
                    <div class="socio">
                        <img src="<?= get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?= get_the_title() ?>">
                        <div class="social">
                            <h3><?= get_the_title() ?></h3>
                            <a class="sociais" target="blank" href="<?= get_field('linkedin') ?>">
                            <i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>
                <?php endwhile; 
                wp_reset_postdata();
            endif; ?>
        </div>
    </section>
</div>
<section class="al-container lp proposta">
    <?php the_content(); ?>
</section>
<div class="background par lp">
    <section class="al-container lp depoimentos">
        <h1 class="title alt"><strong>depoimentos</strong> de nossos acelerados</h1>  
        <?php if( have_rows('depoimentos') ): ?>
            <div class="depoimentos-container">
            <?php while( have_rows('depoimentos') ): the_row(); 
                $imagem = get_sub_field('imagem');?>
                    <div class="depoimento">
                        <img src="<?= get_image_url('quotes.webp')?>" alt="Aspas">
                        <div class="content">
                            <?php the_sub_field('depoimento'); ?>
                        </div>
                        <div class="footer">
                            <div class="left">
                                <h3><?php the_sub_field('nome'); ?></h3>
                                <p><?php the_sub_field('descricao'); ?></p>
                            </div>
                            <div class="right">
                                <div class="img-container">
                                    <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>">
                                </div>  
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </section>
</div>
<div class="back2">
    <section class="al-container lp startup"> 
        <h1 class="title">como aceleramos sua <strong>startup</strong></h1>
        <div class="keywords">
            <span>investimento</span>
            <span>conteúdo</span>
            <span>networking</span>
        </div>
        <?php the_field('aceleramos_sua_startup'); ?>
    </section>
</div>
<div class="back1">
    <section class="al-container lp formulario">
        <div class="header">
            <a href="#form" class="button">Inscreva-se <strong>agora</strong><br>e concorra a <strong>bolsa</strong></a>
            <p>Ao se inscrever o candidato estará concorrendo a bolsas de 100%, 50% e 25%.</p>
            <div class="valores" id="form">
                <div class="left">
                    <p>Valor da aceleração</p>
                </div>
                <div class="right">
                    <p>Participação da<br>AmazonasCap na Startup</p>
                </div>
                <div class="opcao1">
                    <input type="radio" id="quatro" name="drone" value="R$ 3.999,00">
                    <label for="quatro"><span class="left">R$ 3.999,00</span><span class="right">5%</span></label>
                </div>
                <div class="opcao2">
                    <input type="radio" id="sete" name="drone" value="R$ 6.999,00">
                    <label for="sete"><span class="left">R$ 6.999,00</span><span class="right">4%</span></label>
                </div>
                <div class="opcao2">
                    <input type="radio" id="dez" name="drone" value="R$ 9.999,00">
                    <label for="dez"><span class="left">R$ 9.999,00</span><span class="right">3%</span></label>
                </div>
            </div>
        </div>
        <div class="form">
            <?= do_shortcode('[contact-form-7 id="1391" title="Contato LP"]'); ?>
        </div>
    </section>
</div>
<?php get_footer(); ?>
