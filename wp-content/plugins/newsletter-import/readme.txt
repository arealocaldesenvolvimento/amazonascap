=== Import Addon for Newsletter ===
Tags: import
Requires at least: 3.4.0
Tested up to: 5.7.2
Requires PHP: 5.6
Contributors: satollo,webagile,michael-travan

Imports subscriber from CSV files with advanced mapping.

== Description ==

Imports subscriber from CSV files with advanced mapping.

== Changelog ==

= 1.2.4 =

* Typos

= 1.2.3 =

* Correct package

= 1.2.2 =

* Fixed stop button
* Added new comments and notes
* Changed and protexted the importing file folder
* Added new checks on data validity

= 1.2.1 =

* Fix copy and paste UTF-8 encoding

= 1.2.0 =

* Gender import fix
* New notes on fields

= 1.1.8 =

* Added link to log files to check error reasons

= 1.1.7 =

* Added check for write permissions

= 1.1.6 =

* Managed the absence of country even when mapped 

= 1.1.5 =

* Fixed the disappering import background job under some circumstances

= 1.1.4 =

* Added copy and paste import

= 1.1.3 =

* Added quick bounced import
* Improved mapping

= 1.1.2 =

* Fixed access control for editors
* Removed "beta version" notice
* Requires Newsletter 6.9.6+
* Added import for bounced addresses

= 1.1.1 =

* Added 'city' column to import

= 1.1.0 =

* Fixed the status import for new subscribers

= 1.0.9 =

* Added the subscribed import status

= 1.0.8 =

* Improved import for Excel generated files
* Added the field delimiter option

= 1.0.7 =

* Fixed gender import

= 1.0.6 =

* Fixed gender import
* Check on gender and country to avoid database errors
* Report on log files all non conforming lines

= 1.0.5 =

* Added support for language

= 1.0.4 =

* Fixed icon
* Added link to the dcumentation
* New addon format
* Skipped and empty line stats
