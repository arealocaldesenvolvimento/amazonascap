<?php

class NewsletterImport extends NewsletterAddon {

    /**
     * @var NewsletterImport
     */
    static $instance;

    public function __construct($version = '0.0.0') {
        self::$instance = $this;
        parent::__construct('import', $version);
        $this->setup_options();
        add_action('newsletter_import_run', array($this, 'hook_newsletter_import_run'));
    }

    function init() {
        parent::init();

        if ($this->is_allowed()) {
            add_action('admin_menu', array($this, 'hook_admin_menu'), 100);
            add_filter('newsletter_menu_subscribers', array($this, 'hook_newsletter_menu_subscribers'));
        }

        // Try to reschedule if, during update, the job has been eliminated
        if ($this->get_position() !== false && wp_next_scheduled('newsletter_import_run') === false) {
            wp_schedule_event(time() + 10, 'newsletter', 'newsletter_import_run');
        }
    }

    function hook_newsletter_menu_subscribers($entries) {
        $entries[] = array('label' => '<i class="fas fa-database"></i> Advanced Import', 'url' => '?page=newsletter_import_index', 'description' => 'Import subscribers');
        return $entries;
    }

    function hook_admin_menu() {
        add_submenu_page('newsletter_main_index', 'Advanced Import', '<span class="tnp-side-menu">Advanced Import</span>', 'exist', 'newsletter_import_index', function () {
            require __DIR__ . '/index.php';
        });
        add_submenu_page(null, 'CSV import', 'CSV import', 'exist', 'newsletter_import_csv', function () {
            require __DIR__ . '/csv.php';
        });
        add_submenu_page(null, 'Copy&Paste import', 'Copy&Paste import', 'exist', 'newsletter_import_clipboard', function () {
            require __DIR__ . '/clipboard.php';
        });
        add_submenu_page(null, 'Bounce import', 'Bounce import', 'exist', 'newsletter_import_bounce', function () {
            require __DIR__ . '/bounce.php';
        });
    }

    function hook_newsletter_import_run($max_run_time = 240) {

        @setlocale(LC_CTYPE, 'en_US.UTF-8');
        @set_time_limit(0);
        $logger = $this->get_logger();
        //var_dump($logger);
        //die('Siamo qui!');
        $logger->info('Starting');

        // It could have been removed
        $position = $this->get_position();
        if ($position === false) {
            $logger->info('Job stopped');
            return;
        }

        $position = (int) $position;

        $options = $this->options;

        $handle = fopen($this->get_filename(), 'r');
        if (!$handle) {
            $logger->fatal('File disappeared...');
            $this->stop();

            return;
        }

        $stats = get_option('newsletter_import_stats');

        $logger->info('Starting from ' . $position);
        fseek($handle, $position);

        // Skips the first line
        if ($position === 0) {
            fgets($handle);
        }

        $mode = $options['mode'];
        $logger->debug('Mode ' . $mode);

        $newsletter = Newsletter::instance();

        $email_idx = (int) $options['email'] - 1;
        $first_name_idx = (int) $options['first_name'] - 1;
        $last_name_idx = (int) $options['last_name'] - 1;
        $gender_idx = (int) $options['gender'] - 1;
        $country_idx = (int) $options['country'] - 1;
        $region_idx = (int) $options['region'] - 1;
        $ip_idx = (int) $options['ip'] - 1;
        $language_idx = (int) $options['language'] - 1;
        $city_idx = (int) $options['city'] - 1;

        $start = time();
        while (($line = fgets($handle)) !== false && time() < $start + $max_run_time) {
            $this->set_position(ftell($handle));

            $line = trim($line);

            $logger->debug('Processing ' . $line);

            $stats['total']++;

            if (empty($line)) {
                $stats['empty']++;
                update_option('newsletter_import_stats', $stats);
                continue;
            }

            if (!mb_check_encoding($line, "UTF-8")) {
                $logger->fatal('Line ' . $line . ' is not UTF-8 encoded');
                $stats['errors']++;
                update_option('newsletter_import_stats', $stats);
                continue;
            }

            // For fatal logging...
            $text_line = $line;

            //$line = preg_split('/[,;\t]/', $line);
            //$line = array_map('trim', $line);

            $line = str_getcsv($line, $options['delimiter'], '"');
            $email = $newsletter->normalize_email($line[$email_idx]);
            $logger->info('Current email: ' . $email);
            if (!$email) {
                $logger->fatal('Invalid email on line ' . $text_line);
                $stats['errors']++;
                update_option('newsletter_import_stats', $stats);
                continue;
            }

            // Get subscriber

            $subscriber = $newsletter->get_user($email, ARRAY_A);
            if ($subscriber == null) {
                $logger->info('New subscriber');
                $subscriber = array();
                $subscriber['email'] = $email;

                if ($first_name_idx > -1 && isset($line[$first_name_idx])) {
                    $subscriber['name'] = $newsletter->normalize_name($line[$first_name_idx]);
                }

                if ($last_name_idx > -1 && isset($line[$last_name_idx])) {
                    $subscriber['surname'] = $newsletter->normalize_name($line[$last_name_idx]);
                }

                if ($country_idx > -1 && isset($line[$country_idx])) {
                    $country = strtoupper(trim($line[$country_idx]));

                    if (strlen($country) > 2) {
                        $logger->fatal('Country "' . $line[$country_idx] . '" is not 2 digit ISO format on line ' . $text_line);
                        $stats['errors']++;
                        update_option('newsletter_import_stats', $stats);
                        continue;
                    }
                    $subscriber['country'] = $country;
                }

                if ($region_idx > -1 && isset($line[$region_idx])) {
                    $subscriber['region'] = trim($line[$region_idx]);
                }

                if ($ip_idx > -1 && isset($line[$ip_idx])) {
                    $subscriber['ip'] = trim($line[$ip_idx]);
                }

                if ($language_idx > -1 && isset($line[$language_idx])) {
                    $subscriber['language'] = strtolower(trim($line[$language_idx]));
                }

                if ($gender_idx > -1 && isset($line[$gender_idx])) {
                    $gender = strtolower(trim($line[$gender_idx]));
                    if (!empty($gender)) {
                        $gender = substr($gender, 0, 1);
                        if ($gender != 'f' && $gender != 'm' && $gender != 'n') {
                            $logger->fatal('Gender is invalid on line ' . $text_line);
                            $stats['errors']++;
                            update_option('newsletter_import_stats', $stats);
                            continue;
                        }
                        $subscriber['sex'] = $gender;
                    }
                }

                if ($city_idx > -1 && isset($line[$city_idx])) {
                    $subscriber['city'] = trim($line[$city_idx]);
                }

                $subscriber['status'] = $options['import_as'];

                foreach ($options['lists'] as $i) {
                    $subscriber['list_' . $i] = 1;
                }

                $profiles = $newsletter->get_profiles();
                foreach ($profiles as $profile) {
                    $profile_idx = (int) $options['profile_' . $profile->id] - 1;
                    if ($profile_idx > -1 && isset($line[$profile_idx])) {
                        $subscriber['profile_' . $profile->id] = $line[$profile_idx];
                    }
                }

                $newsletter->save_user($subscriber);
                $stats['new']++;
                //$logger->debug($subscriber);
            } else {
                $logger->info('Matched subscriber ID ' . $subscriber['id']);
                if ($mode == 'skip') {
                    $logger->info('Skipped');
                    $stats['skipped']++;
                    update_option('newsletter_import_stats', $stats);
                    continue;
                }

                if ($mode == 'overwrite') {
                    // Clean up the subscriber
                    $subscriber['name'] = '';
                    $subscriber['surname'] = '';
                    $subscriber['sex'] = 'n';
                    for ($i = 1; $i <= NEWSLETTER_PROFILE_MAX; $i++) {
                        $subscriber['profile_' . $i] = '';
                    }
                    for ($i = 1; $i <= NEWSLETTER_LIST_MAX; $i++) {
                        $subscriber['list_' . $i] = 0;
                    }
                    $subscriber['country'] = '';
                    $subscriber['region'] = '';
                    $subscriber['city'] = '';
                }

                if ($language_idx > -1 && isset($line[$language_idx])) {
                    $subscriber['language'] = strtolower(trim($line[$language_idx]));
                }

                if ($first_name_idx > -1) {
                    $subscriber['name'] = $newsletter->normalize_name($line[$first_name_idx]);
                }
                if ($last_name_idx > -1) {
                    $subscriber['surname'] = $newsletter->normalize_name($line[$last_name_idx]);
                }
                if ($gender_idx > -1) {
                    $gender = strtolower($line[$gender_idx]);
                    if (!empty($gender)) {
                        $gender = substr($gender, 0, 1);
                        if ($gender != 'f' && $gender != 'm' && $gender != 'n') {
                            $logger->fatal('Gender is invalid on line ' . $text_line);
                            $stats['errors']++;
                            update_option('newsletter_import_stats', $stats);
                            continue;
                        }
                        $subscriber['sex'] = $gender;
                    }
                }
                if (isset($options['override_status'])) {
                    $subscriber['status'] = $options['import_as'];
                }
                if ($country_idx > -1 && isset($line[$country_idx])) {
                    if (strlen($line[$country_idx]) > 2) {
                        $logger->fatal('Country "' . $line[$country_idx] . '" is not 2 digit ISO format on line ' . $text_line);
                        $stats['errors']++;
                        update_option('newsletter_import_stats', $stats);
                        continue;
                    }
                    $subscriber['country'] = $line[$country_idx];
                }
                if ($region_idx > -1 && isset($line[$region_idx])) {
                    $subscriber['region'] = $line[$region_idx];
                }
                if ($city_idx > -1 && isset($line[$city_idx])) {
                    $subscriber['city'] = $line[$city_idx];
                }
                foreach ($options['lists'] as $i) {
                    $subscriber['list_' . $i] = 1;
                }

                $profiles = $newsletter->get_profiles();
                foreach ($profiles as $profile) {
                    $profile_idx = (int) $options['profile_' . $profile->id] - 1;
                    if ($profile_idx > -1 && isset($line[$profile_idx])) {
                        $subscriber['profile_' . $profile->id] = $line[$profile_idx];
                    }
                }

                //$logger->debug($subscriber);
                $newsletter->save_user($subscriber);
                $stats['updated']++;
            }
            update_option('newsletter_import_stats', $stats);

            // TODO: Remove is for test
            //break;
        }
        fclose($handle);
        if ($line === false) {
            $logger->info('Finished');
            $this->stop();
        } else {
            $logger->info('To be continued');
        }
    }

    function stop() {
        $this->delete_file();
        delete_option('newsletter_import_position');
        wp_clear_scheduled_hook('newsletter_import_run');
    }

    function start() {
        $this->set_position(0);
        wp_schedule_event(time() + 10, 'newsletter', 'newsletter_import_run');
        update_option('newsletter_import_stats', array('total' => 0, 'errors' => 0, 'new' => 0, 'updated' => 0, 'skipped' => 0, 'empty' => 0), false);
    }

    function is_importing() {
        return $this->get_position() !== false;
    }

    function get_position() {
        return get_option('newsletter_import_position');
    }

    function set_position($position) {
        update_option('newsletter_import_position', $position, false);
    }

    function has_file() {
        return file_exists($this->get_filename());
    }

    function get_filename() {
        return $this->get_dir() . '/import.csv';
    }

    function get_dir() {
        $dir = wp_upload_dir();
        return $dir['basedir'] . '/newsletter/import';
    }

    function prepare_dir() {
        $dir = $this->get_dir();
        wp_mkdir_p($dir);
        if (!is_writable($dir) || !is_readable($dir)) {
            return false;
        }
        
        // Old file name (version < 1.2.2)
        @unlink(dirname($dir) . '/import.csv');
        
        file_put_contents($dir . '/.htaccess', "<IfModule mod_authz_core.c>\nRequire all denied\n</IfModule>\n<IfModule !mod_authz_core.c>\nOrder deny,allow\nDeny from all\n</IfModule>");
        return true;
    }

    function get_progress() {
        $position = get_option('newsletter_import_position');
        $total = filesize($this->get_filename());
        return (int) ($position * 100 / $total);
    }

    function delete_file() {
        $fn = $this->get_filename();
        if (file_exists($fn)) {
            return unlink($fn);
        }
    }

}
