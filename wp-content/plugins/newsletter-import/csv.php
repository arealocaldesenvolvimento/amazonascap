<?php
/* @var $this NewsletterImport */

defined('ABSPATH') || exit;

global $wpdb;

@include_once NEWSLETTER_INCLUDES_DIR . '/controls.php';
$controls = new NewsletterControls();

$return_page = 'newsletter_import_csv';

if ($this->is_importing()) {
    include __DIR__ . '/csv-importing.php';
    return;
}

if ($this->has_file()) {
    include __DIR__ . '/csv-map.php';
    return;
}

if (!$controls->is_action()) {
    $controls->data = $this->options;
    if (!$this->prepare_dir()) {
        $controls->errors .= 'The folder ' . esc_html($this->get_dir()) . ' cannot be created or is not writable or is not readable. Ask for support to your hosting provider.';
    }
} else {

    if ($controls->is_action('import')) {
        if (is_uploaded_file($_FILES['file']['tmp_name'])) {
            $this->stop();
            $r = move_uploaded_file($_FILES['file']['tmp_name'], $this->get_filename());
            if ($r === false) {
                $controls->errors = 'The file cannor be copied in the folder ' . esc_html($dir) . '/newsletter. Check if it exists and is writeable. You can also ask for support to your hosting provider.';
            }
            else {
                $controls->js_redirect('admin.php?page=newsletter_import_csv');
            }
        }
        return;
    }
}
?>

<div class="wrap" id="tnp-wrap">

    <?php include NEWSLETTER_DIR . '/tnp-header.php'; ?>

    <div id="tnp-heading">
        <h3>Advanced import</h3>
        <h2>1. File upload</h2>
        <?php $controls->panel_help('https://www.thenewsletterplugin.com/documentation/addons/extended-features/advanced-import/') ?>
    </div>

    <div id="tnp-body">

        <form method="post" action="" enctype="multipart/form-data">
            <?php $controls->init(); ?>

            <table class="form-table">
                <tr>
                    <th>
                        <?php _e('CSV file', 'newsletter') ?>
                    </th>
                    <td>
                        <input type="file" name="file" />

                        <?php $controls->button('import', 'Next'); ?>

                        <p class="description">
                            The file <strong>must be UTF-8 encoded</strong>, be sure to export it in that format. For Excel, choose "File/Save as" to have the option to
                            select that format.
                        </p>
                    </td>
                </tr>
            </table>

            <?php include __DIR__ . '/last-import-statistics.php'; ?>

        </form>
    </div>

    <?php include NEWSLETTER_DIR . '/tnp-footer.php'; ?>

</div>
