<?php
/* @var $this NewsletterImport */

defined('ABSPATH') || exit;

global $wpdb;

@include_once NEWSLETTER_INCLUDES_DIR . '/controls.php';
$module = NewsletterImport::$instance;
$controls = new NewsletterControls();

if (!$controls->is_action()) {
    $controls->data = $module->options;
    if (!$this->prepare_dir()) {
        $controls->errors .= 'The folder ' . esc_html($this->get_dir()) . ' cannot be created or is not writable or is not readable. Ask for support to your hosting provider.';
    }
} else {

    if ($controls->is_action('import-from-clipboard')) {

        //Normalize pasted string
        $data = preg_replace('/\t/', ',', stripslashes($_POST['pasted_text']));
        $data = str_replace("\r\n", "\n", $data);
        
        //$data = utf8_encode( $data );

        file_put_contents($this->get_filename(), $data);
        $controls->js_redirect('admin.php?page=newsletter_import_csv');
    }
}
?>

<div class="wrap" id="tnp-wrap">

    <?php include NEWSLETTER_DIR . '/tnp-header.php'; ?>

    <div id="tnp-heading">
        <h3>Advanced import</h3>
        <h2>1. Copy&Paste Import</h2>
        <?php $controls->panel_help('https://www.thenewsletterplugin.com/documentation/addons/extended-features/advanced-import/') ?>
    </div>

    <div id="tnp-body">

        <form method="post" action="">
            <?php $controls->init(); ?>
            
            <p>Note: the first line MUST contain field labels, like:</p>
            <p>
                <code>Email;First Name; Last Name</code>
            </p>
            <p>the order is not important on nect screen you can map your data columns to the subscriber fields.</p>

            <table class="form-table">
                <tr>
                    <th>
                        <?php _e('Copy & Paste', 'newsletter') ?>
                    </th>
                    <td>
                        <textarea name="pasted_text" style="width: 100%; height: 200px; font-size: 11px; font-family: monospace"></textarea>
                        <?php $controls->button('import-from-clipboard', 'Next'); ?>
                    </td>
                </tr>

            </table>

            <?php include __DIR__ . DIRECTORY_SEPARATOR . 'last-import-statistics.php'; ?>

        </form>
    </div>

    <?php include NEWSLETTER_DIR . '/tnp-footer.php'; ?>

</div>
