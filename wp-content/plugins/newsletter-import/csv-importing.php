<?php
defined('ABSPATH') || exit;

/* @var $this NewsletterImport */
/* @var $controls NewsletterControls */

if ($controls->is_action('stop')) {
    $this->stop();
    $controls->js_redirect('admin.php?page=newsletter_import_index');
}

if ($controls->is_action('refresh')) {
    // just a reload
}
?>

<div class="wrap" id="tnp-wrap">

    <?php include NEWSLETTER_DIR . '/tnp-header.php'; ?>

    <div id="tnp-heading">
        <h3>Advanced import</h3>
        <h2>3. The import is running in background</h2>
        <?php $controls->panel_help('https://www.thenewsletterplugin.com/documentation/addons/extended-features/advanced-import/') ?>
    </div>

    <div id="tnp-body">

        <form method="post" action="#">
            <?php $controls->init(); ?>

            <p>
                Importing is undergoing (<?php echo $this->get_progress() ?>%)<br>
                
                Next importer run on <?php echo $controls->print_date(wp_next_scheduled('newsletter_import_run'), false, true) ?>
            </p>

            <p>
                <?php $controls->button_confirm('stop', 'Stop'); ?>
                <?php $controls->button('refresh', 'Refresh'); ?>
            </p>

        </form>
    </div>

    <?php include NEWSLETTER_DIR . '/tnp-footer.php'; ?>

</div>
